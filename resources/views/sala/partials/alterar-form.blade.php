@include('includes.errors')

<form action = "{{ action('AdminController@postAlterarLocal', $local->id) }}" method="post">
  {!! csrf_field() !!}

  <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label col-sm-6">
    <input class="mdl-textfield__input" type="text" name="nome" id="nome" value="{{ old("nome", $local->nome) }}"/>
    <label class="mdl-textfield__label" for="nome">Nome</label>
  </div>

  <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label col-sm-6">
    <input class="mdl-textfield__input" type="number" name="capacidade" id="capacidade" value="{{ old("capacidade", $local->capacidade) }}"/>
    <label class="mdl-textfield__label" for="capacidade">Capacidade</label>
  </div>

  <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label col-sm-12">
    <label class="mdl-textfield__label" for="descricao">Descrição</label>
    <textarea class="mdl-textfield__input" name="descricao" id="descricao">{{ old("descricao", $local->descricao) }}</textarea>
  </div>

  <button class="login-button col-sm-offset-3 col-sm-6 mdl-button mdl-js-button mdl-button--raised mdl-button--colored mdl-js-ripple-effect" id="salvar-dados" type="submit">
    Salvar
  </button>
  <div class="clear"></div>
</form>

