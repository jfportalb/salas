@extends('templates.admin.main')

@section('title', 'Alterar Local')

@section('css')
  <link rel="stylesheet" href="{{ asset('css/user/alterar.css') }}">
@endsection

@section('content')
  <div class="container">
    <div class="login-card white col-sm-offset-2 col-sm-8 mdl-shadow--2dp">

      <div class="title-text center col-xs-12">
        <h2><b>Alterar Local</b></h2>
        <div class="clear"></div>
      </div>

      @include('sala.partials.alterar-form', $local)

      <div class="clear"></div>
    </div>
  </div>

@endsection

@section('script')

@endsection