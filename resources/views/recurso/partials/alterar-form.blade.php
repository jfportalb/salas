@include('includes.errors')

<form action="{{ action('RecursosController@postAlterar', $recurso->id) }}" method="post">
  {{csrf_field()}}

  <div class="col-xs-12 input-field mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
    <input class="mdl-textfield__input" type="text" name="nome" id="nome" value="{{ old('nome', $recurso->nome)}}"/>
    <label class="mdl-textfield__label" for="nome">Nome</label>
  </div>
  <div class="col-xs-12 input-field mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
    <textarea class="mdl-textfield__input" rows= "3" id="descricao" name="descricao" >{{ old('descricao', $recurso->descricao)}}</textarea>
    <label class="mdl-textfield__label" for="descricao">Descrição</label>
  </div>

  <input type="submit" class="login-button col-sm-offset-2 col-sm-8 mdl-button mdl-js-button mdl-button--raised
					mdl-button--colored mdl-js-ripple-effect a-text" value="Adicionar">

  <div class="clear"></div>
</form>