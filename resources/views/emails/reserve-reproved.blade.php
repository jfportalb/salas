<!doctype html>
<html lang="pt-BR">
<head>
    <meta charset="UTF-8">
</head>
<body>
<h2>Olá {{$usuario->nome}},</h2>
<p>Infelizmente o seu pedido {{$pedido->title}} de reserva para a sala <strong>{{$pedido->sala->nome}}</strong>
    para as reservas abaixo foi eprovado no Sistema de Reserva de Salas do NCE.</p>
<li>
    @foreach($pedido->reservas as $reserva)
        <ul>De {{$reserva->start}} até {{$reserva->end}}</ul>
    @endforeach
</li>
</body>
</html>
