<!doctype html>
<html lang = "pt-BR">
  <head>
    <meta charset = "UTF-8">
  </head>
  <body>
    <h2>Olá {{$nome}}</h2>
    <p>Seu cadastro no Sistema de Reserva de Salas do NCE foi reprovado.</p>
    <p>Caso tenha alguma dúvida entre em contato com: suporte{{'@'}}nce.ufrj.br</p>
  </body>
</html>
