<!doctype html>
<html lang = "pt-BR">
  <head>
    <meta charset = "UTF-8">
  </head>
  <body>
    <h2>Olá {{ $nome }}</h2>
    <p>Seu cadastro foi feito no Sistema de Reserva de Salas do NCE.</p>
    <p>seu login é {{ $email }} e para definir sua senha clique <a href="{{ action('UserController@getCriarSenha') }}">aqui</a></p>
  </body>
</html>