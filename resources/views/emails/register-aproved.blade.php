<!doctype html>
<html lang = "pt-BR">
  <head>
    <meta charset = "UTF-8">
  </head>
  <body>
    <h2>Olá {{$nome}}</h2>
    <p>Seu cadastro no Sistema de Reserva de Salas do NCE foi aprovado.</p>
    <p>Para acessar o sistema utilize o link: {{ action('Auth\AuthController@getLogin') }}</p>
  </body>
</html>
