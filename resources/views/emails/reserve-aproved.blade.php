<!doctype html>
<html lang = "pt-BR">
  <head>
    <meta charset = "UTF-8">
  </head>
  <body>
    <h2>Olá {{$usuario->nome}},</h2>
    <p>O seu pedido de reserva {{$pedido->title}} para a sala <strong>{{$pedido->sala->nome}}</strong>
        foi aprovado no Sistema de Reserva de Salas do NCE.</p>
    <li>
        @foreach($pedido->reservas as $reserva)
            <ul>De {{$reserva->start}} até {{$reserva->end}}</ul>
        @endforeach
    </li>
  </body>
</html>
