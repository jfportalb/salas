<!doctype html>
<html lang="pt-BR">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link rel="shortcut icon" href="{{asset("images/logos/tab.png")}}">

    <title>@yield("title")</title>

    <link rel="stylesheet" href="{!!asset('libs/mdl/material.min.css')!!}">
    <link href="{{asset('libs/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">

    @yield("css")

    <link rel="stylesheet" href="{!!asset('css/main.css')!!}">
</head>
<body>
    @yield("content")

    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="{!!asset('libs/mdl/material.min.js')!!}"></script>
    <script src="{{asset("libs/bootstrap/js/bootstrap.min.js")}}"></script>


    @yield("popups")

    @yield("script")

</body>
</html>