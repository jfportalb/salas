<!-- Modal -->
<div class="modal fade" id="criar_area_modal" tabindex="-1" role="dialog" aria-labelledby="alterarDadosModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="center modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="alterarDadosModal">Adicionar Área</h4>
            </div>
            <div class="modal-body">

                @include('includes.errors')

                <form action="{{ action('AdminController@postCriarArea') }}" method="post">
                    {{csrf_field()}}

                    <div class="col-xs-12 input-field mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                        <input class="mdl-textfield__input" type="text" name="nome" id="nome" value="{{ old('nome')}}"/>
                        <label class="mdl-textfield__label" for="nome">Nome</label>
                    </div>

                    <input type="submit" class="login-button col-sm-offset-2 col-sm-8 mdl-button mdl-js-button mdl-button--raised
					mdl-button--colored mdl-js-ripple-effect a-text" value="Adicionar">

                    <div class="clear"></div>
                </form>
            </div>
        </div>
    </div>
</div>