<div class="modal fade" id="local" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="center modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel"><span id="sala_nome"></span></h4>
            </div>
            <div class="modal-body">
                <b>Área:</b> <span id="sala_area"></span><br/>
                <b>Capacidade:</b> <span id="sala_capacidade"></span> pessoas.<br/><br/>
                <b>Descrição:</b> <span id="sala_descricao"></span><br/><br/>
                <b>Recursos Disponíveis:</b><br/>
                <span id="sala_recursos">
                </span>
                <br/><b>Administradores:</b><br/>
                <span id="sala_adms">
                </span>

            </div>
        </div>
    </div>
</div>