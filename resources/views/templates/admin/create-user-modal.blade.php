<!-- Modal -->
<div class="modal fade" id="criar_user_modal" tabindex="-1" role="dialog" aria-labelledby="alterarDadosModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="center modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="alterarDadosModal">Adicionar Administrador</h4>
            </div>
            <div class="modal-body">
                @include('includes.errors')
                @include('includes.error')
                @include('includes.status')

                <form action="" method="post">
                    {{csrf_field()}}

                    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label col-sm-12">
                        <input class="mdl-textfield__input" type="text" name="nome" id="nome" value="{{ old('nome') }}"/>
                        <label class="mdl-textfield__label" for="nome">Nome</label>
                    </div>

                    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label col-sm-6">
                        <input class="mdl-textfield__input" type="text" name="email" id="email" value="{{ old('email') }}"/>
                        <label class="mdl-textfield__label" for="email">E-mail</label>
                    </div>

                    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label col-sm-6">
                        <input class="mdl-textfield__input" type="text" name="celular" id="celular" value="{{ old('celular') }}"/>
                        <label class="mdl-textfield__label" for="celular">Celular</label>
                    </div>

                    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label col-sm-6">
                        <input class="mdl-textfield__input" type="number" name="ramal" id="ramal" value="{{ old('ramal') }}"/>
                        <label class="mdl-textfield__label" for="ramal">Ramal NCE</label>
                    </div>

                    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label col-sm-6">
                        <input class="mdl-textfield__input" type="text" name="sala" id="sala" value="{{ old('sala') }}"/>
                        <label class="mdl-textfield__label" for="sala">Sala</label>
                    </div>

                    <button class="login-button col-sm-offset-3 col-sm-6 mdl-button mdl-js-button mdl-button--raised mdl-button--colored mdl-js-ripple-effect" type="submit">
                        Cadastrar
                    </button>

                </form>
                <div class="clear"></div>

            </div>
        </div>
    </div>
</div>