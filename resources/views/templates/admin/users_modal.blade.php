<div class="modal fade" id="user" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="center modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel"><span id="user_nome"></span></h4>
            </div>
            <div class="modal-body">
                <b>Email:</b> <span id="user_email"></span><br/><br/>
                <b>Sala:</b> <span id="user_sala"></span><br/><br/>
                <b>Ramal:</b> <span id="user_ramal"></span><br/><br/>
                <b>Celular:</b><span id="user_celular"></span><br/>
                <br/><b>Reservas:</b><br/>
                <div>
                    {{--Ainda não definimos como fazer isso--}}
                    <div class="clear"></div>
                </div>

            </div>
        </div>
    </div>
</div>