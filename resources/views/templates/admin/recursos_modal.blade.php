<div class="modal fade" id="recurso" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="center modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel"><span id="recurso_nome"></span></h4>
            </div>
            <div class="modal-body">
                <b>Descrição:</b> <span id="recurso_descricao"></span><br/><br/>
                <form action="{{route('criarRecurso')}}" id="new_recurso_form" method="post">
                    {{method_field('PUT')}}
                    <b>Adicionar novo:</b>
                    {{csrf_field()}}
                    <input type="text" name="id" id="rec_id" hidden>
                    <input type="text" id="new_recurso" name="codigo">
                    <button type="submit" class="table-button button mdl-button mdl-js-button
                    mdl-button--raised mdl-button--colored mdl-js-ripple-effect a-text">
                        Adicionar
                    </button><br/><br/>
                </form>
                <b>Recursos existentes:</b><br>
                <span id="recursos"></span>
                <br/><br/>

            </div>
        </div>
    </div>
</div>