<!-- Modal -->
<div class="modal fade" id="criar_local_modal" tabindex="-1" role="dialog" aria-labelledby="alterarDadosModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="center modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="alterarDadosModal">Adicionar Sala</h4>
            </div>
            <div class="modal-body">

                @include('includes.errors')

                <form action="{{ action('AdminController@postCriarLocal') }}" method="post">
                    {{csrf_field()}}

                    <input type="hidden" name="area_id" id="area_id" value="{{old("area_id")}}">

                    <div class="col-xs-12 input-field mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                        <input class="mdl-textfield__input" type="text" name="nome" id="nome" value="{{ old('nome')}}"/>
                        <label class="mdl-textfield__label" for="nome">Nome</label>
                    </div>
                    <div class="col-xs-12 input-field mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                        <input class="mdl-textfield__input" type="number" name="capacidade" id="capacidade" value="{{ old('capacidade')}}"/>
                        <label class="mdl-textfield__label" for="capacidade">Capacidade de pessoas</label>
                    </div>
                    <div class="col-xs-12 input-field mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                        <textarea class="mdl-textfield__input" type="text" rows= "3" id="descricao" name="descricao" >{{ old('descricao')}}</textarea>
                        <label class="mdl-textfield__label" for="descricao">Descrição</label>
                    </div>

                    <input type="submit" class="login-button col-sm-offset-2 col-sm-8 mdl-button mdl-js-button mdl-button--raised
					mdl-button--colored mdl-js-ripple-effect a-text" value="Adicionar">

                    <div class="clear"></div>
                </form>
            </div>
        </div>
    </div>
</div>