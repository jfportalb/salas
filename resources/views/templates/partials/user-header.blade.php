<header class="mdl-layout__header mdl-layout__header--scroll">
    <div class="mdl-layout__header-row">
        <!-- Logo -->
        <div class="logo-box">
            <a href="{{ url('/user/home') }}">
                <img class="logo" src="{!!asset('images/logos/tab.png')!!}" alt="">
            </a>
        </div>
        <!-- Title -->
        <span class="mdl-layout-title">Reserva de salas</span>
        <!-- Add spacer, to align navigation to the right -->
        <div class="mdl-layout-spacer"></div>
        <!-- Navigation -->
        <nav class="mdl-navigation">
            <a class="mdl-navigation__link" href="{{ url('/user/pedidos') }}">Meus Pedidos</a>
            <a class="mdl-navigation__link" href="{{ url('/user/home') }}">Reservar Sala</a>
            <a class="mdl-navigation__link" href="{{ action('UserController@getAlterar', ['id' => Auth::user()->id]) }}">Alterar Dados</a>
            <a class="mdl-navigation__link" href="{{ action('Auth\AuthController@getLogout') }}">Sair</a>
        </nav>
    </div>
</header>

<div class="mdl-layout__drawer mdl-layout--small-screen-only">
    <nav class="mdl-navigation">
        <a class="mdl-navigation__link" href="{{ url('/user/pedidos') }}">Meus Pedidos</a>
        <a class="mdl-navigation__link" href="{{ url('/user/home') }}">Reservar Sala</a>
        <a class="mdl-navigation__link" href="{{ action('UserController@getAlterar', ['id' => Auth::user()->id]) }} }}">Alterar Dados</a>
        <a class="mdl-navigation__link" href="{{ action('Auth\AuthController@getLogout') }}">Sair</a>
    </nav>
</div>
