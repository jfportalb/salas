<!doctype html>
<html lang="pt-BR">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="shortcut icon" href="{{asset("images/logos/tab.png")}}">

    <title>@yield("title")</title>

    <link rel="stylesheet" href="{!!asset('libs/mdl/material.min.css')!!}">
    <link rel="stylesheet" href="//fonts.googleapis.com/icon?family=Material+Icons">
    <link href="{{asset('libs/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:300,400,500,700" type="text/css">
    <link rel="stylesheet" href="{!!asset('css/main.css')!!}">

    @if(Auth::user()->tipo_login == App\User::TYPE_USUARIO)
      <link rel="stylesheet" href="{!!asset('css/user/main.css')!!}">
    @else
      <link rel="stylesheet" href="{!!asset('css/admin/main.css')!!}">
    @endif

    @yield("css")
  </head>
  <body>

    <div class="mdl-layout mdl-js-layout">
      
      @if(Auth::user()->tipo_login == App\User::TYPE_USUARIO)
        @include('templates.partials.user-header')
      @else
        @include('templates.partials.admin-header')
      @endif

      <main class="mdl-layout__content">
        <div class="page-content">
          @yield("content")
        </div>
      </main>

      @include('templates.partials.footer')
    </div>

    @yield("popups")

    {{--<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>--}}
    <script src="{!!asset('libs/jquery/jquery-1.11.2.min.js')!!}"></script>
    <script src="{!!asset('libs/mdl/material.min.js')!!}"></script>
    <script src="{{asset("libs/bootstrap/js/bootstrap.min.js")}}"></script>
    <script src="{{asset("libs/dateFormat.js")}}"></script>

    @yield("script")
  </body>
</html>