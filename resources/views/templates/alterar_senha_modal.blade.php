<!-- Modal -->
<div class="modal fade" id="senha" tabindex="-10" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="center modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Alterar Senha</h4>
            </div>
            <div class="modal-body">
                @include('includes.errors')

                <form action="{{ action('UserController@postAlterarSenha', ['id' => Auth::user()->id]) }}" method="post">
                    {{csrf_field()}}

                    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label col-sm-12">
                        <input class="mdl-textfield__input" type="password" name="old_password" id="old_password"/>
                        <label class="mdl-textfield__label" for="old_password">Senha atual</label>
                    </div>

                    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label col-sm-12">
                        <input class="mdl-textfield__input" type="password" name="password" id="password"/>
                        <label class="mdl-textfield__label" for="password">Nova senha</label>
                    </div>

                    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label col-sm-12">
                        <input class="mdl-textfield__input" type="password" name="password_confirmation" id="password_confirmation"/>
                        <label class="mdl-textfield__label" for="password_confirmation">Confirmar nova senha</label>
                    </div>

                    <button class="login-button col-sm-offset-3 col-sm-6 mdl-button mdl-js-button mdl-button--raised mdl-button--colored mdl-js-ripple-effect" type="submit">
                        Alterar
                    </button>
                </form>
                <div class="clear"></div>
            </div>
        </div>
    </div>
</div>