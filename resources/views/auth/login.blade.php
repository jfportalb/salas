@extends('templates.main')
@section('title','Reserva de Salas - login')

@section("css")
    <link rel="stylesheet" href="{!!asset('css/login.css')!!}">
@endsection

@section('content')

    <div class="container">
        <div class="login-card white col-sm-offset-3 col-sm-6 mdl-shadow--2dp">

            <img class="logo" src="{!!asset('images/logos/logo_extended2.png')!!}" alt="">

            @include('includes.errors')
            @include('includes.status')

            <form action="{{ action('Auth\AuthController@postLogin') }}" method="post">
                {{csrf_field()}}

                <div class="input-field mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                    <input class="mdl-textfield__input" type="text" name="email" id="email" value="{{ old('email')}}"/>
                    <label class="mdl-textfield__label" for="email">E-mail</label>
                </div>

                <div class="input-field mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                    <input class="mdl-textfield__input" type="password" name="password" id="password"/>
                    <label class="mdl-textfield__label" for="password">Senha</label>
                </div>

                <div>
                    <input class="login-button col-sm-5 mdl-button
                    mdl-js-button mdl-button--raised mdl-button--colored
                    mdl-js-ripple-effect" type="submit" value="Entrar"/>


					{{--<a class="login-button col-sm-offset-2 col-sm-5 mdl-button mdl-js-button mdl-button--raised--}}
					{{--mdl-button--colored mdl-js-ripple-effect a-text" href="{{ action('Auth\AuthController@getRegister') }}">Cadastre-se</a>--}}

                </div>
                <div class="clear"></div>
            </form>

            <div class="esqueci">
                <a href="{{ action('Auth\PasswordController@getEmail') }}">Esqueci minha senha</a>
            </div>
        </div>

        <div class="col-sm-12 dev">

            <div class="col-sm-offset-5 col-sm-2">
                Desenvolvido Por:
                <a href="http://ejcm.com.br/"><img class="logo" src="{{asset("images/ejcm.png")}}" alt=""></a>
            </div>

        </div>


    </div>

    <!-- Modal -->
    <div class="modal fade" id="created" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="center modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Conta Criada Com Sucesso!</h4>
                </div>
                <div class="modal-body">
                    Enviaremos um email quando sua conta for aprovada!
                </div>
                <div class="modal-footer">
                    <button data-dismiss="modal" class="login-button mdl-button mdl-js-button mdl-button--raised mdl-button--colored mdl-js-ripple-effect">Ok</button>

                </div>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="redefinir" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="center modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Redefinição de Senha</h4>
                </div>
                <div class="modal-body">
                    Foi enviado um email com instruções para redefinir sua senha!
                </div>
                <div class="modal-footer">
                    <button data-dismiss="modal" class="login-button mdl-button mdl-js-button mdl-button--raised mdl-button--colored mdl-js-ripple-effect">Ok</button>

                </div>
            </div>
        </div>
    </div>

@endsection

@section('script')

    @if( Session::has("created") )
        <script>
            $('#created').modal('show');
        </script>
    @endif

    @if( Session::has("redefinir") )
        <script>
            $('#redefinir').modal('show');
        </script>
    @endif

@endsection