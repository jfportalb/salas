@extends('templates.main')
@section('title','Reserva de Salas - Redefinir senha')

@section("css")
	<link rel="stylesheet" href="{!!asset('css/esqueci.css')!!}">
@endsection

@section('content')

	<div class="container">
		<div class="login-card white col-sm-offset-3 col-sm-6 mdl-shadow--2dp">
			<div class="logo-box col-sm-offset-3 col-sm-6">
				<a href="{{URL('/')}}"><img class="logo" src="{!!asset('images/logos/logo_extended2.png')!!}" alt="Núcleo de Computação Eletrônica"></a>
			</div>

			<div class="center col-sm-12">
				<h3>Redefinir Senha</h3>
				<div class="clear"></div>
			</div>

			@include('includes.errors')
			@include('includes.status')

			<form method="POST" action="{{ action('Auth\PasswordController@postReset') }}">
				{!! csrf_field() !!}

				<input type="hidden" name="token" value="{{ $token }}">

				<div class="input-field mdl-textfield mdl-js-textfield mdl-textfield--floating-label col-sm-12">
					<input class="mdl-textfield__input" type="text" name="email" id="email" value="{{ old('email') }}"/>
					<label class="mdl-textfield__label" for="email">E-mail</label>
				</div>

				<div class="input-field mdl-textfield mdl-js-textfield mdl-textfield--floating-label col-sm-12">
					<input class="mdl-textfield__input" type="password" name="password" id="password" value="{{ old('password') }}"/>
					<label class="mdl-textfield__label" for="email">Nova Senha</label>
				</div>

				<div class="input-field mdl-textfield mdl-js-textfield mdl-textfield--floating-label col-sm-12">
					<input class="mdl-textfield__input" type="password" name="password_confirmation" id="password_confirmation" value="{{ old('password_confirmation') }}"/>
					<label class="mdl-textfield__label" for="email">Confirmar Nova Senha</label>
				</div>

				<button class="login-button col-sm-offset-3 col-sm-6 mdl-button mdl-js-button mdl-button--raised mdl-button--colored mdl-js-ripple-effect" type="submit">
					Enviar
				</button>
			</form>

		</div>
	</div>
@endsection

@section('popups')

	@include('templates.user.pop_up_senha_redefinida')

@endsection

@section('script')

	@if( Session::has("password_changed") )
		<script>
			$('#password_changed').modal('show');
		</script>
	@endif

@endsection
