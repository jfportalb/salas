@extends('templates.main')
@section('title','Reserva de Salas - Cadastro')

@section("css")
    <link rel="stylesheet" href="{!!asset('css/cadastro.css')!!}">
@endsection

@section('content')

    <div class="container">
        <div class="login-card white col-sm-offset-2 col-sm-8 mdl-shadow--2dp">

            <div class="logo-box col-sm-offset-3 col-sm-6">
                <a href="{{URL('/')}}"><img class="logo" src="{!!asset('images/logos/logo_extended2.png')!!}"
                                            alt="Núcleo de Computação Eletrônica"></a>
            </div>

            <div class="center col-sm-12">
                <h3>Cadastro</h3>
            </div>

            @if(!$errors->isEmpty())

                <div class = "col-sm-12 nce-red mdl-shadow--2dp">
                    @foreach($errors->all() as $error)
                        <p class="mdl-color-text--white mdl-typography--text-center mdl-cell mdl-cell--12-col">{{$error}}</p>
                    @endforeach
                </div>

            @endif

            <form action="" method="post">
                {{csrf_field()}}

                <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label col-sm-12">
                    <input class="mdl-textfield__input" type="text" name="nome" id="nome" value="{{ old('nome') }}"/>
                    <label class="mdl-textfield__label" for="nome">Nome</label>
                </div>

                <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label col-sm-6">
                    <input class="mdl-textfield__input" type="text" name="email" id="email" value="{{ old('email') }}"/>
                    <label class="mdl-textfield__label" for="email">E-mail</label>
                </div>

                <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label col-sm-6">
                    <input class="mdl-textfield__input" type="text" name="celular" id="celular" value="{{ old('celular') }}"/>
                    <label class="mdl-textfield__label" for="celular">Celular</label>
                </div>

                <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label col-sm-6">
                    <input class="mdl-textfield__input" type="number" name="ramal" id="ramal" value="{{ old('ramal') }}"/>
                    <label class="mdl-textfield__label" for="ramal">Ramal NCE</label>
                </div>

                <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label col-sm-6">
                    <input class="mdl-textfield__input" type="text" name="sala" id="sala" value="{{ old('sala') }}"/>
                    <label class="mdl-textfield__label" for="sala">Sala</label>
                </div>

                <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label col-sm-6">
                    <input class="mdl-textfield__input" type="password" name="password" id="password"/>
                    <label class="mdl-textfield__label" for="password">Senha</label>
                </div>

                <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label col-sm-6">
                    <input class="mdl-textfield__input" type="password" name="password_confirmation" id="password_confirmation"/>
                    <label class="mdl-textfield__label" for="password_confirmation">Confirmar senha</label>
                </div>

                <button class="login-button col-sm-offset-3 col-sm-6 mdl-button mdl-js-button mdl-button--raised mdl-button--colored mdl-js-ripple-effect" type="submit">
                    Cadastrar
                </button>

            </form>
        </div>

        <div class="col-sm-12 dev">

            <div class="col-sm-offset-5 col-sm-2">
                Desenvolvido Por:
                <a href="http://ejcm.com.br/"><img class="logo" src="{{asset("images/ejcm.png")}}" alt=""></a>
            </div>

        </div>

    </div>



@endsection