@if(Session::has('status'))
  <div class="col-sm-12 nce-red alert-success mdl-shadow--2dp">
    {{ Session::get('status') }}
  </div>
@endif