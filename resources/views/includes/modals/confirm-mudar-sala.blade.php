<div class="modal fade" id="confirmacao" tabindex="-1" role="dialog" aria-labelledby="confirmarDeletarUsuario">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="center modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                Você irá perder todas as reservas selecionadas. <br>
                Deseja mesmo prosseguir?
            </div>
            <div class="modal-footer">
                <button data-dismiss="modal" class="col-xs-5 login-button mdl-button
        mdl-js-button mdl-button--raised mdl-button--colored
        mdl-js-ripple-effect" id="cancelar">Cancelar
                </button>
                <button data-dismiss="modal" class="col-xs-5 col-xs-offset-2
        login-button
        mdl-button
        mdl-js-button mdl-button--raised mdl-button--colored
        mdl-js-ripple-effect" id="aprovar">Sim
                </button>
            </div>
        </div>
    </div>
</div>