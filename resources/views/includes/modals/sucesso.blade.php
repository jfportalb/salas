<div class="modal fade" id="sucesso" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="center modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Sucesso!</h4>
            </div>
            <div class="modal-body">{{ Session::get('sucesso') }}</div>
            <div class="modal-footer">
                <button data-dismiss="modal" class="login-button mdl-button mdl-js-button mdl-button--raised mdl-button--colored mdl-js-ripple-effect">Ok</button>
            </div>
        </div>
    </div>
</div>