@if(!$errors->isEmpty())
  <div class = "col-sm-12 nce-red mdl-shadow--2dp">
    @foreach($errors->all() as $error)
      <p class="mdl-color-text--white mdl-typography--text-center mdl-cell mdl-cell--12-col">{{$error}}</p>
    @endforeach
  </div>
@endif