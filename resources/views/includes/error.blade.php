@if(Session::has('error'))
  <div class = "col-sm-12 nce-red mdl-shadow--2dp">
      <p class="mdl-color-text--white mdl-typography--text-center mdl-cell mdl-cell--12-col">{{ Session::get('error') }}</p>
  </div>
@endif