@extends('templates.main')
@section('title','Reserva de Salas - Cadastro')

@section("css")
  <link rel="stylesheet" href="{!!asset('css/cadastro.css')!!}">
@endsection

@section('content')

  <div class="container">
    <div class="login-card white col-sm-offset-2 col-sm-8 mdl-shadow--2dp">

      <div class="logo-box col-sm-offset-3 col-sm-6">
        <a href="{{URL('/')}}"><img class="logo" src="{!!asset('images/logos/logo_extended2.png')!!}"
                                    alt="Núcleo de Computação Eletrônica"></a>
      </div>

      <div class="center col-sm-12">
        <h3>Criar Senha</h3>
      </div>

      @include('includes.errors')
      @include('includes.error')
      @include('includes.status')

      <form action="" method="post">
        {{csrf_field()}}

        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label col-sm-12">
          <input class="mdl-textfield__input" type="text" name="email" id="email" value="{{ old('email') }}"/>
          <label class="mdl-textfield__label" for="email">E-mail</label>
        </div>

        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label col-sm-6">
          <input class="mdl-textfield__input" type="password" name="senha" id="senha"/>
          <label class="mdl-textfield__label" for="senha">Senha</label>
        </div>

        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label col-sm-6">
          <input class="mdl-textfield__input" type="password" name="senha_confirmation" id="senha_confirmation"/>
          <label class="mdl-textfield__label" for="senha_confirmation">Confirmar senha</label>
        </div>

        <button class="login-button col-sm-offset-3 col-sm-6 mdl-button mdl-js-button mdl-button--raised mdl-button--colored mdl-js-ripple-effect" type="submit">
          Enviar
        </button>

      </form>
    </div>

    <div class="col-sm-12 dev">

      <div class="col-sm-offset-5 col-sm-2">
        Desenvolvido Por:
        <a href="http://ejcm.com.br/"><img class="logo" src="{{asset("images/ejcm.png")}}" alt=""></a>
      </div>

    </div>

  </div>

@endsection

@section('popups')

  @include('templates.mensagem_sucesso')

@endsection

@section('script')

  @if( Session::has('sucesso') )
    <script>
      $('#sucesso').modal('show');
    </script>
  @endif

@endsection
