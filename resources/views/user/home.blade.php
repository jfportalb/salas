@extends('templates.dashboard')

@section('title','Página Inicial')

@section("css")
    <link rel="stylesheet" href="{!!asset('css/user/home.css')!!}">
    <link rel='stylesheet' href='{{asset('libs/fullcalendar-2.6.0/fullcalendar.css')}}' />
    <link rel='stylesheet' href='{{asset('libs/mdl-selectfield/mdl-selectfield.min.css')}}' />
@endsection

@section('content')

    <div class="container calendario-div">
        <div class="col-sm-8">
            <div id='calendar'></div>
        </div>
        <div class="col-sm-4">
            <div class="container reservas-box demo-card-square mdl-card
            mdl-shadow--2dp">
                @include('user.partials.criar-reserva-form')
            </div>
        </div>
    </div>
    <hr>

@endsection

@section('popups')

    @include('templates.day_modal')
    @include('includes.modals.sucesso')
    @include('includes.modals.confirm-mudar-sala')

@endsection

@section('script')
    
    <script src="{{asset("libs/fullcalendar-2.6.0/lib/moment.min.js")}}"></script>
    <script src="{{asset("libs/fullcalendar-2.6.0/fullcalendar.js")}}"></script>
    <script src='{{asset("libs/fullcalendar-2.6.0/lang/pt-br.js")}}'></script>
    <script src="{{asset("libs/fullcalendar-2.6.0/gcal.js")}}"></script>
    <script src="{{asset('libs/mdl-selectfield/mdl-selectfield.min.js')}}"></script>
    <script src="{{asset("scripts/calendar.js")}}"></script>

@endsection
