<h3>Novo Pedido</h3>

@include('includes.errors')
{{csrf_field()}}

<form id="pedido-form" action="{{ route('criar-reserva') }}" method="post">


    <div class="col-xs-12 mdl-selectfield mdl-js-selectfield mdl-js-textfield mdl-selectfield--floating-label">
        <select class="mdl-selectfield__select" name="local" id="local" required>
            <option disabled selected>Escolha sua sala</option>
            @foreach($salas as $area)
                <optgroup label="{{$area[0]->area->nome}}">
                    @foreach($area as $sala)
                        <option value="{{ $sala->id }}">
                            {{$sala -> nome}}
                        </option>
                    @endforeach
                </optgroup>
            @endforeach
        </select>
        <label class="mdl-selectfield__label mdl-textfield__label" for="local">{{ucfirst("local")}}</label>
    </div>
    <div id="pedidos-input" style="display: none">

        <div class="col-xs-12 input-field mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
            <input class="mdl-textfield__input" type="text" name="titulo" id="titulo" value="{{old("titulo")}}"
                   required/>
            <label class="mdl-textfield__label" for="titulo">{{ucfirst("titulo")}}</label>
        </div>

        <div id="reservas">

        </div>

        <div class="col-xs-12 input-field mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
            <textarea class="mdl-textfield__input" rows="3" id="descricao" name="descricao" required></textarea>
            <label class="mdl-textfield__label" for="descricao">Descrição</label>
        </div>

        <button class="login-button col-sm-offset-3 col-sm-6 mdl-button mdl-js-button mdl-button--raised mdl-button--colored mdl-js-ripple-effect"
                id="salvar-dados" type="submit">
            Salvar
        </button>
    </div>
</form>

