@extends('templates.dashboard')

@section('title','Reserva de Salas - Alterar Dados')

@section("css")
    <link rel="stylesheet" href="{!!asset('css/user/alterar.css')!!}">
@endsection

@section('content')

    <div class="container">
        <div class="login-card white col-sm-offset-2 col-sm-8 mdl-shadow--2dp">

            <div class="title-text center col-xs-12">
                <h2><b>Alterar Dados</b></h2>
                <div class="clear"></div>
            </div>

            @if(Session::has('nome'))
                <div class = "col-sm-12 nce-red mdl-shadow--2dp">
                    <p class="mdl-color-text--white mdl-typography--text-center mdl-cell mdl-cell--12-col">{{ Session::get('error') }}</p>
                </div>
            @endif
            @if(Session::has('email'))
                <div class = "col-sm-12 nce-red mdl-shadow--2dp">
                    <p class="mdl-color-text--white mdl-typography--text-center mdl-cell mdl-cell--12-col">{{ Session::get('error') }}</p>
                </div>
            @endif
            @if(Session::has('celular'))
                <div class = "col-sm-12 nce-red mdl-shadow--2dp">
                    <p class="mdl-color-text--white mdl-typography--text-center mdl-cell mdl-cell--12-col">{{ Session::get('error') }}</p>
                </div>
            @endif
            @if(Session::has('ramal'))
                <div class = "col-sm-12 nce-red mdl-shadow--2dp">
                    <p class="mdl-color-text--white mdl-typography--text-center mdl-cell mdl-cell--12-col">{{ Session::get('error') }}</p>
                </div>
            @endif
            @if(Session::has('sala'))
                <div class = "col-sm-12 nce-red mdl-shadow--2dp">
                    <p class="mdl-color-text--white mdl-typography--text-center mdl-cell mdl-cell--12-col">{{ Session::get('error') }}</p>
                </div>
            @endif

            <form action="" method="post">

                {{csrf_field()}}

                <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label col-sm-12">
                    <input class="mdl-textfield__input" type="text" name="nome" id="nome" value="{{{Input::old("nome", $usuario->nome)}}}"/>
                    <label class="mdl-textfield__label" for="nome">Nome</label>
                </div>

                <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label col-sm-6">
                    <input class="mdl-textfield__input" type="text" name="email" id="email" value="{{{Input::old("email", $usuario->email)}}}"/>
                    <label class="mdl-textfield__label" for="email">E-mail</label>
                </div>

                <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label col-sm-6">
                    <input class="mdl-textfield__input" type="text" name="celular" id="celular" value="{{{Input::old("celular", $usuario->celular)}}}"/>
                    <label class="mdl-textfield__label" for="celular">Celular</label>
                </div>

                <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label col-sm-6">
                    <input class="mdl-textfield__input" type="number" name="ramal" id="ramal" value="{{{Input::old("ramal", $usuario->ramal)}}}"/>
                    <label class="mdl-textfield__label" for="ramal">Ramal NCE</label>
                </div>

                <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label col-sm-6">
                    <input class="mdl-textfield__input" type="text" name="sala" id="sala" value="{{{Input::old("sala", $usuario->sala)}}}"/>
                    <label class="mdl-textfield__label" for="sala">Sala</label>
                </div>

                <button class="login-button col-sm-offset-3 col-sm-6 mdl-button mdl-js-button mdl-button--raised mdl-button--colored mdl-js-ripple-effect" type="submit">
                    Salvar
                </button>

            </form>

            <button onclick="showAlterarSenhaModal()" class="senha-button col-sm-offset-3 col-sm-6 mdl-button mdl-js-button mdl-button--raised mdl-button--colored mdl-js-ripple-effect" type="submit">
                Alterar Senha
            </button>


        </div>
    </div>

@endsection

@section('popups')

    @include('templates.alterar_senha_modal')
    @include('includes.modals.sucesso')
    @include('templates.user.pop_up_senha_redefinida')

@endsection

@section('script')

    <script>
        function showAlterarSenhaModal() {
            $('#senha').modal('show');
        }
    </script>

    @if( Session::has("perfil_alterado") )
        <script>
            $('#perfil_alterado').modal('show');
        </script>
    @endif

    @if( Session::has("password_changed") )
        <script>
            $('#password_changed').modal('show');
        </script>
    @endif

    @if( Session::has("password_not_changed") )
        <script>
            showAlterarSenhaModal();
        </script>
    @endif

@endsection
