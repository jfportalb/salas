@extends('templates.dashboard')

@section('title','Meus Pedidos')

@section("css")
    <link rel="stylesheet" href="{!!asset('css/admin/listar.css')!!}">
    <link rel='stylesheet' href='{{asset('libs/mdl-selectfield/mdl-selectfield.min.css')}}'/>
@endsection

@section('content')
    <div class="container">
        <div class="login-card">
            <div class="title-text center col-xs-12">
                <h2><b>Meus Pedidos</b></h2>
                <div class="clear"></div>
            </div>

            <div class="col-sm-12 row">
                <div class="col-sm-12">
                    <h3><b>Pedidos pendentes</b></h3>
                </div>
            </div>
            <div class="col-xs-12 mdl-data-table mdl-js-data-table mdl-shadow--2dp">
                <div class="s-dont row table-row">
                    <div class="col-sm-3">
                        <b>Nome</b>
                    </div>
                    <div class="col-sm-2">
                        <b>Sala</b>
                    </div>
                    <div class="col-sm-4">
                        <b>Descrição</b>
                    </div>
                    <div class="col-sm-3">
                        <div class="right">
                            <b>Ação</b>
                        </div>
                    </div>
                </div>
                @if(isset($pedidos["0"]))
                    @foreach($pedidos["0"] as $pedido)
                        <div class="pedido item-container row table-row-2">
                            <input type = "hidden" value="{{ $pedido->id }}" name="pedido-id">
                            <div class="col-sm-3">
                                {{ $pedido->title }}
                            </div>
                            <div class="col-sm-2">
                                {{ $pedido->sala->nome }}
                            </div>
                            <div class="col-sm-4">
                                {{ $pedido->descricao }}
                            </div>
                            <div class="col-sm-3">
                                <div class="right">
                                    <a class="deletar table-button button mdl-button mdl-js-button
                            mdl-button--raised mdl-button--colored mdl-js-ripple-effect a-text"
                                       href="{{ route('cancelarPedido', ['id' => $pedido->id]) }}">
                                        Cancelar pedido
                                    </a>
                                </div>
                            </div>
                        </div>
                    @endforeach
                @endif
            </div>

            <div class="col-sm-12 row">
                <div class="col-sm-12">
                    <h3><b>Pedidos aprovados</b></h3>
                </div>
            </div>
            <div class="col-xs-12 mdl-data-table mdl-js-data-table mdl-shadow--2dp">
                <div class="s-dont row table-row">
                    <div class="col-sm-3">
                        <b>Nome</b>
                    </div>
                    <div class="col-sm-2">
                        <b>Sala</b>
                    </div>
                    <div class="col-sm-4">
                        <b>Descrição</b>
                    </div>
                    <div class="col-sm-3">
                        <div class="right">
                            <b>Ação</b>
                        </div>
                    </div>
                </div>
                @if(isset($pedidos["1"]))
                    @foreach($pedidos["1"] as $pedido)
                        <div class="pedido item-container row table-row-2">
                            <input type = "hidden" value="{{ $pedido->id }}" name="pedido-id">
                            <div class="col-sm-3">
                                {{ $pedido->title }}
                            </div>
                            <div class="col-sm-2">
                                {{ $pedido->sala->nome }}
                            </div>
                            <div class="col-sm-4">
                                {{ $pedido->descricao }}
                            </div>
                            <div class="col-sm-3">
                                <div class="right">
                                    <a class="deletar table-button button mdl-button mdl-js-button
                            mdl-button--raised mdl-button--colored mdl-js-ripple-effect a-text"
                                       href="{{ route('cancelarPedido', ['id' => $pedido->id]) }}">
                                        Cancelar pedido
                                    </a>
                                </div>
                            </div>
                        </div>
                    @endforeach
                @endif
            </div>

            <div class="col-sm-12 row">
                <div class="col-sm-12">
                    <h3><b>Pedidos recusados</b></h3>
                </div>
            </div>
            <div class="col-xs-12 mdl-data-table mdl-js-data-table mdl-shadow--2dp">
                <div class="s-dont row table-row">
                    <div class="col-sm-3">
                        <b>Nome</b>
                    </div>
                    <div class="col-sm-2">
                        <b>Sala</b>
                    </div>
                    <div class="col-sm-4">
                        <b>Descrição</b>
                    </div>
                    <div class="col-sm-3">
                        <div class="right">
                            <b>Ação</b>
                        </div>
                    </div>
                </div>
                @if(isset($pedidos["2"]))
                    @foreach($pedidos["2"] as $pedido)
                        <div class="pedido item-container row table-row-2">
                            <input type = "hidden" value="{{ $pedido->id }}" name="pedido-id">
                            <div class="col-sm-3">
                                {{ $pedido->title }}
                            </div>
                            <div class="col-sm-2">
                                {{ $pedido->sala->nome }}
                            </div>
                            <div class="col-sm-4">
                                {{ $pedido->descricao }}
                            </div>
                            <div class="col-sm-3">
                                <div class="right">
                                    <a data-item="{{ $pedido->title }}" class="deletar table-button button mdl-button mdl-js-button
                mdl-button--raised mdl-button--colored mdl-js-ripple-effect a-text"
                                       href="{{ route('cancelarPedido', ['id' => $pedido->id]) }}">
                                        Apagar pedido
                                    </a>
                                </div>
                            </div>
                        </div>
                    @endforeach
                @endif
            </div>
        </div>
    </div>
@endsection

@section('popups')

    @include('includes.modals.confirm', ['item' => 'esta reserva'])
    @include('includes.modals.sucesso')
    @include('admin.modal-pedido')

@endsection

@section('script')
    <script src="{{ asset('scripts/meus-pedidos.js') }}"></script>
    <script src="{{ asset('scripts/remover-item.js') }}"></script>
@endsection
