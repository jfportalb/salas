@extends('templates.dashboard')
@section('title','Contas Cadastradas')

@section("css")
    <link rel="stylesheet" href="{!!asset('css/admin/listar.css')!!}">
    <link rel="stylesheet" href="{!!asset('css/user/alterar.css')!!}">

@endsection

@section('content')
    <div class="container">
        <div class="login-card">
            <div class="title-text center col-xs-12">
                <h2><b>Usuários Cadastrados</b></h2>
                <div class="clear"></div>
            </div>

            <div class="row">
                <div class="col-xs-offset-2 col-xs-8 col-sm-offset-4 col-sm-4">
                    <a data-toggle="modal" data-target="#criar_user_modal" class="button-area button-add button mdl-button mdl-js-button mdl-button--raised mdl-button--colored mdl-js-ripple-effect a-text" href="">
                        Adicionar Usuário
                    </a>
                </div>
            </div>



            @include('includes.status')

            <div class="col-xs-12 mdl-data-table mdl-js-data-table mdl-shadow--2dp">
                <div class="s-dont row table-row">
                    <div class="col-sm-3">
                        <b>Nome Completo</b>
                    </div>
                    <div class="col-sm-3">
                        <b>Email</b>
                    </div>
                    <div class="col-sm-3">
                        <b>Celular</b>
                    </div>
                    <div class="col-sm-3">
                        <div class="right">
                            <b>Ação</b>
                        </div>
                    </div>
                </div>
                @foreach($usuarios as $usuario)
                    @if($usuario->tipo_login == 0)
                        <div class="item-container row table-row-2 user" id="user_{{$usuario->id}}">
                            <div class="col-sm-3">
                                {{ $usuario->nome }}
                            </div>
                            <div class="col-sm-3">
                                {{ $usuario->email }}
                            </div>
                            <div class="col-sm-3">
                                {{ $usuario->celular }}
                            </div>
                            <div class="col-sm-3">
                                <div class="right">
                                    <a data-item="Usuário" class="alterar table-button button mdl-button mdl-js-button
                            mdl-button--raised mdl-button--colored mdl-js-ripple-effect a-text"
                                       href="{{ action('UserController@getAlterar', ['id' => $usuario->id]) }}">
                                        Alterar
                                    </a>
                                    <a class="deletar table-button button mdl-button mdl-js-button
                            mdl-button--raised mdl-button--colored mdl-js-ripple-effect a-text"
                                       href="{{ action('UserController@getDeletar', ['id' => $usuario->id]) }}">
                                        Deletar
                                    </a>
                                </div>
                            </div>
                        </div>
                    @endif
                @endforeach

            </div>

            {!! $usuarios->render() !!}

            <div class="clear"></div>

        </div>
    </div>
@endsection

@section('popups')

    @include('includes.modals.confirm', ['item' => 'este usuário'])
    @include('includes.modals.alterar-item')
    @include('includes.modals.sucesso')
    @include('templates.alterar_senha_modal')
    @include('templates.user.pop_up_senha_redefinida')
    @include('templates.admin.users_modal')
    @include('templates.admin.create-user-modal')

@endsection

@section('script')

    <script src="{{ asset('scripts/remover-item.js') }}"></script>
    <script src="{{ asset('scripts/alterar-item.js') }}"></script>
    <script src="{{ asset('scripts/admin-usuarios.js') }}"></script>

    @if( Session::has("sucesso") )
        <script>
            $('#sucesso').modal('show');
        </script>
    @endif

    @if( Session::has("password_changed") )
        <script>
            $('#password_changed').modal('show');
        </script>
    @endif

    @if( Session::has("password_not_changed") )
        <script>
            showAlterarSenhaModal();
        </script>
    @endif

    @if( Session::has("create_user_failed") )
        <script>
            $('#criar_user_modal').modal('show');
        </script>
    @endif

@endsection