@extends('templates.dashboard')

@section('title','Alterar Dados')

@section("css")
    <link rel="stylesheet" href="{!!asset('css/user/alterar.css')!!}">
@endsection

@section('content')

    <div class="container">
        <div class="login-card white col-sm-offset-2 col-sm-8 mdl-shadow--2dp">

            <div class="title-text center col-xs-12">
                <h2><b>Alterar Dados</b></h2>
                <div class="clear"></div>
            </div>

            @include('admin.partials.alterar-form', $usuario)

        </div>
    </div>



@endsection

@section('popups')

    @include('includes.modals.sucesso')
    @include('templates.alterar_senha_modal')
    @include('templates.user.pop_up_senha_redefinida')

@endsection

@section('script')

    <script>
        function showAlterarSenhaModal() {
            $('#senha').modal('show');
        }
    </script>

    @if( Session::has("sucesso") )
        <script>
            $('#sucesso').modal('show');
        </script>
    @endif

    @if( Session::has("password_changed") )
        <script>
            $('#password_changed').modal('show');
        </script>
    @endif

    @if( Session::has("password_not_changed") )
        <script>
            showAlterarSenhaModal();
        </script>
    @endif

@endsection
