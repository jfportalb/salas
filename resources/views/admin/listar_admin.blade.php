@extends('templates.dashboard')
@section('title','Contas Cadastradas')

@section("css")
    <link rel="stylesheet" href="{!!asset('css/admin/listar.css')!!}">
    <link rel="stylesheet" href="{!!asset('css/user/alterar.css')!!}">

@endsection

@section('content')
    <div class="container">
        <div class="login-card">
            <div class="title-text center col-xs-12">
                <h2><b>Administradores Cadastrados</b></h2>
                <div class="clear"></div>
            </div>

            @if(Auth::user()->isSuperAdmin())
                <div class="col-sm-12 row">
                    <div class="col-sm-12">
                        <h3><b>Super Administradores</b></h3>
                    </div>
                    <div class="col-sm-12">
                        <a data-toggle="modal" data-target="#criar_SuperAdmin_modal" class="button-add button mdl-button mdl-js-button mdl-button--raised
                    mdl-button--colored mdl-js-ripple-effect a-text">
                            Adicionar Super Administrador
                        </a>
                    </div>
                </div>

                <div class="col-xs-12 mdl-data-table mdl-js-data-table mdl-shadow--2dp">
                    <div class="s-dont row table-row">
                        <div class="col-sm-3">
                            <b>Nome Completo</b>
                        </div>
                        <div class="col-sm-3">
                            <b>Email</b>
                        </div>
                        <div class="col-sm-3">
                            <b>Celular</b>
                        </div>
                        <div class="col-sm-3">
                            <div class="right">
                                <b>Ação</b>
                            </div>
                        </div>
                    </div>
                    @foreach($usuarios as $usuario)
                        @if($usuario->tipo_login == 2 )
                            <div class="item-container row table-row-2">
                                <div class="col-sm-3">
                                    {{ $usuario->nome }}
                                </div>
                                <div class="col-sm-3">
                                    {{ $usuario->email }}
                                </div>
                                <div class="col-sm-3">
                                    {{ $usuario->celular }}
                                </div>
                                <div class="col-sm-3">
                                    <div class="right">
                                        <a data-item="Super Administrador" class="alterar table-button button mdl-button mdl-js-button
                            mdl-button--raised mdl-button--colored mdl-js-ripple-effect a-text"
                                           href="{{ action('UserController@getAlterar', ['id' => $usuario->id]) }}">
                                            Alterar
                                        </a>
                                        @if($usuario->id != Auth::user()->id)
                                            <a class="deletar table-button button mdl-button mdl-js-button
                            mdl-button--raised mdl-button--colored mdl-js-ripple-effect a-text"
                                               href="{{ action('UserController@getDeletar', ['id' => $usuario->id]) }}">
                                                Deletar
                                            </a>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        @endif
                    @endforeach
                </div>

                <div class="clear"></div>
            @endif
            <div class="col-sm-12 row">
                <div class="col-sm-12">
                    <h3><b>Administradores</b></h3>
                </div>
                <div class="col-sm-12">
                    <a data-toggle="modal" data-target="#criar_admin_modal" class="button-add button mdl-button mdl-js-button mdl-button--raised
                    mdl-button--colored mdl-js-ripple-effect a-text">
                        Adicionar Administrador
                    </a>
                </div>
            </div>

            <div class="col-xs-12 mdl-data-table mdl-js-data-table mdl-shadow--2dp">
                <div class="s-dont row table-row">
                    <div class="col-sm-3">
                        <b>Nome Completo</b>
                    </div>
                    <div class="col-sm-3">
                        <b>Email</b>
                    </div>
                    <div class="col-sm-3">
                        <b>Celular</b>
                    </div>
                    <div class="col-sm-3">
                        <div class="right">
                            <b>Ação</b>
                        </div>
                    </div>
                </div>
                @foreach($usuarios as $usuario)
                    @if($usuario->tipo_login == 1)
                        <div class="item-container row table-row-2">
                            <div class="col-sm-3">
                                {{ $usuario->nome }}
                            </div>
                            <div class="col-sm-3">
                                {{ $usuario->email }}
                            </div>
                            <div class="col-sm-3">
                                {{ $usuario->celular }}
                            </div>
                            <div class="col-sm-3">
                                <div class="right">
                                    <a data-item="Administrador" class="alterar table-button button mdl-button mdl-js-button
                            mdl-button--raised mdl-button--colored mdl-js-ripple-effect a-text"
                                       href="{{ action('UserController@getAlterar', ['id' => $usuario->id]) }}">
                                        Alterar
                                    </a>
                                    @if($usuario->id != Auth::user()->id)
                                        <a class="deletar table-button button mdl-button mdl-js-button
                            mdl-button--raised mdl-button--colored mdl-js-ripple-effect a-text"
                                           href="{{ action('UserController@getDeletar', ['id' => $usuario->id]) }}">
                                            Deletar
                                        </a>
                                    @endif
                                </div>
                            </div>
                        </div>
                    @endif
                @endforeach
            </div>

            <div class="clear"></div>

        </div>
    </div>
@endsection

@section('popups')

    @include('includes.modals.confirm', ['item' => 'este usuário'])
    @include('templates.alterar_senha_modal')
    @include('templates.user.pop_up_senha_redefinida')
    @include('includes.modals.alterar-item')
    @include('templates.admin.create-admin-modal')
    @include('templates.admin.create-SuperAdmin-modal')
    @include('includes.modals.sucesso')

@endsection

@section('script')

    <script src="{{ asset('scripts/remover-item.js') }}"></script>
    <script src="{{ asset('scripts/alterar-item.js') }}"></script>

    <script>
        function showAlterarSenhaModal() {
            $('#senha').modal('show');
        }
    </script>

    @if( Session::has("sucesso") )
        <script>
            $('#sucesso').modal('show');
        </script>
    @endif

    @if( Session::has("password_changed") )
        <script>
            $('#password_changed').modal('show');
        </script>
    @endif

    @if( Session::has("password_not_changed") )
        <script>
            showAlterarSenhaModal();
        </script>
    @endif

    @if( Session::has("create_admin_failed") )
        <script>
            $('#criar_admin_modal').modal('show');
        </script>
    @endif

    @if( Session::has("create_SuperAdmin_failed") )
        <script>
            $('#criar_SuperAdmin_modal').modal('show');
        </script>
    @endif

@endsection