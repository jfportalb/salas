@extends('templates.dashboard')
@section('title','Lista de Salas')

@section("css")
    <link rel="stylesheet" href="{!!asset('css/admin/listar.css')!!}">
    <link rel="stylesheet" href="{!!asset('css/admin/local.css')!!}">
@endsection

@section('content')

    <div class="container">
        <div class="login-card">
            <div class="title-text center col-xs-12">
                <h2><b>Salas Adicionadas</b></h2>
                <div class="clear"></div>
            </div>

            <div class="row">
                <div class="col-xs-offset-2 col-xs-8 col-sm-offset-4 col-sm-4">
                    <button data-toggle="modal" data-target="#criar_area_modal" class="button-area button-add button mdl-button mdl-js-button mdl-button--raised mdl-button--colored mdl-js-ripple-effect a-text">
                        Adicionar Área
                    </button>
                </div>
            </div>
            @if(count($areas) > 0)
                @foreach($areas as $area)
                    <div class="item-container area-box container">
                        <div class="area-box-header col-sm-12 row">
                            <div class="col-sm-12" >
                                <h3><b>{{$area->nome}}</b> </h3>
                            </div>
                            <div class="col-sm-12" >
                                <a data-item="Área" class="alterar button-add button
                                mdl-button mdl-js-button mdl-button--raised mdl-button--colored mdl-js-ripple-effect a-text"
                                   href="{{ action('AdminController@getAlterarArea', $area->id) }}">
                                    Alterar
                                </a>
                                <a data-item="esta área e todos seus locais" class="deletar button-delete button-add button mdl-button mdl-js-button mdl-button--raised
                                mdl-button--colored mdl-js-ripple-effect a-text" href="{{ action('AdminController@getDeletarArea', $area->id) }}">
                                    Deletar
                                </a>
                            </div>
                        </div>


                        <div class="col-xs-12 mdl-data-table mdl-js-data-table mdl-shadow--2dp">
                            <div class="s-dont row table-row">
                                <div class="col-sm-4">
                                    Nome
                                </div>
                                <div class="col-sm-4">
                                    Tipo
                                </div>
                            </div>
                            @foreach($area->salas as $sala)

                                <div class="item-container row table-row-2 sala" id="sala_{{$sala->id}}">
                                    <div class="col-sm-4">
                                        {{ $sala->nome }}
                                    </div>
                                    <div class="col-sm-4">
                                        {{ $sala->tipo }}
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="right">
                                            <a data-item="Local" class="alterar table-button button mdl-button mdl-js-button mdl-button--raised mdl-button--colored
                                            mdl-js-ripple-effect a-text" href="{{ action('AdminController@getAlterarLocal', $sala->id) }}">
                                                Alterar
                                            </a>

                                            <a class="deletar table-button button mdl-button mdl-js-button mdl-button--raised mdl-button--colored
                                        mdl-js-ripple-effect a-text" href="{{ action('AdminController@getDeletarLocal', $sala->id) }}">
                                                Deletar
                                            </a>
                                        </div>
                                    </div>
                                </div>

                            @endforeach
                        </div>

                        <a id="{{$area->id}}" data-toggle="modal" data-target="#criar_local_modal" class="button-add-local button-local button mdl-button mdl-js-button mdl-button--raised mdl-button--colored mdl-js-ripple-effect a-text" href="">
                            Adicionar Sala
                        </a>

                        <div class="clear"></div>
                    </div>
                @endforeach
            @endif
        </div>
    </div>

@endsection

@section('popups')

    @include('includes.modals.confirm', ['item' => 'este local'])
    @include('includes.modals.alterar-item')
    @include('includes.modals.sucesso')
    @if(count($areas))
        @include('templates.admin.locais_modal')
    @endif
    @include('templates.admin.create-area-modal')
    @include('templates.admin.create-local-modal')

@endsection

@section('script')

    <script src="{{ asset('scripts/admin-locais.js') }}"></script>

    <script src="{{ asset('scripts/remover-item.js') }}"></script>
    <script src="{{ asset('scripts/alterar-item.js') }}"></script>

    @if( Session::has("sucesso") )
        <script>
            $('#sucesso').modal('show');
        </script>
    @endif

    @if( Session::has("criar-area-fail") )
        <script>
            $('#criar_area_modal').modal('show');
        </script>
    @endif

    @if( Session::has("criar-local-fail") )
        <script>
            $('#criar_local_modal').modal('show');
        </script>
    @endif

@endsection