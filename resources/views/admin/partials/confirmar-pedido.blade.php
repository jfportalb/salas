<div class="pedido-box">
    <h3>Atenção</h3>
    <h5>Ao confirmar este pedido você estará recusando o(s) seguinte(s) pedido(s) devido à concorrência de
        horários:</h5>
    @foreach($pedidos as $pedido)
        <hr>
        <p>Dono: {{ $pedido->user->nome }}</p>
        <p>Sala: {{ $pedido->sala->nome }}</p>
        <p>Descrição: {{ $pedido->descricao }}</p>
        <p>Reservas:</p>
        <div class="reservas-itens">
            @foreach($pedido->reservas as $reserva)
                <p>De {{ $reserva->start }} até {{ $reserva->end }}</p>
            @endforeach
        </div>
    @endforeach
    <div class="modal-footer">
        <div class="status-button button-div col-xs-4">
            <input type="hidden" name="pendencia_id" value="{{ $pedido_id }}">
            <a class="mdl-button mdl-button--colored mdl-js-button mdl-js-ripple-effect" id="aprovar-confirmed">
                Aprovar
                <input name="status" value="aprovar" style="display:none;"/>
                <input type="checkbox" name="confirm" checked style="display:none;"/>
            </a>
        </div>
        <div class="status-button button-div col-xs-offset-4  col-xs-4">
            <a data-dismiss="modal" class="right mdl-button mdl-js-button mdl-js-ripple-effect">
                Cancelar
            </a>
        </div>
    </div>
</div>