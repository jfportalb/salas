@include('includes.errors')

<form action="{{ action('UserController@postAlterar', $usuario->id) }}" method="post">
  {{csrf_field()}}

  <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label col-sm-12">
    <input class="mdl-textfield__input" type="text" name="nome" id="nome" value="{{ old("nome", $usuario->nome) }}"/>
    <label class="mdl-textfield__label" for="nome">Nome</label>
  </div>

  <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label col-sm-6">
    <input class="mdl-textfield__input" type="text" name="email" id="email" value="{{ old("email", $usuario->email) }}"/>
    <label class="mdl-textfield__label" for="email">E-mail</label>
  </div>

  <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label col-sm-6">
    <input class="mdl-textfield__input" type="number" name="celular" id="celular" value="{{ old("celular", $usuario->celular) }}"/>
    <label class="mdl-textfield__label" for="celular">Celular</label>
  </div>

  <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label col-sm-6">
    <input class="mdl-textfield__input" type="number" name="ramal" id="ramal" value="{{ old("ramal", $usuario->ramal) }}"/>
    <label class="mdl-textfield__label" for="ramal">Ramal NCE</label>
  </div>

  <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label col-sm-6">
    <input class="mdl-textfield__input" type="text" name="sala" id="sala" value="{{ old("sala", $usuario->sala) }}"/>
    <label class="mdl-textfield__label" for="sala">Sala</label>
  </div>

  <button class="login-button col-sm-offset-3 col-sm-6 mdl-button mdl-js-button mdl-button--raised mdl-button--colored mdl-js-ripple-effect" id="salvar-dados" type="submit">
    Salvar
  </button>
</form>

@if(Auth::user()->id == $usuario->id)
  <button onclick="showAlterarSenhaModal()" class="senha-button col-sm-offset-3 col-sm-6 mdl-button mdl-js-button mdl-button--raised mdl-button--colored mdl-js-ripple-effect" type="submit">
    Alterar Senha
  </button>
@endif

<div class="clear"></div>

