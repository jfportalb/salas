<div class="pedido-box">
  <p>Dono: {{ $pedido->user->nome }}</p>
  <p>Sala: {{ $pedido->sala->nome }}</p>
  <p>Descrição: {{ $pedido->descricao }}</p>
  <p>Reservas:</p>
  <div class="reservas-itens">
    @foreach($pedido->reservas as $reserva)
      <p>De {{ $reserva->start }} até {{ $reserva->end }}</p>
    @endforeach
  </div>
</div>