<div class="modal fade" id="recusa" tabindex="-1" role="dialog" aria-labelledby="confirmarDeletarUsuario">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="center modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
            </div>
            <form id="recusar" method="post" onsubmit="">
                {{csrf_field()}}
                <div class="modal-body">
                    <h4>Indique o motivo da recusa:</h4>
                    <textarea name="reason" id="reason" cols="30" rows="10"></textarea>
                </div>
                <div class="modal-footer">
                    <button data-dismiss="modal" class="col-xs-5 login-button mdl-button
        mdl-js-button mdl-button--raised mdl-button--colored
        mdl-js-ripple-effect" id="cancelar">Cancelar
                    </button>
                    <button class="col-xs-5 col-xs-offset-2 login-button
        mdl-button mdl-js-button mdl-button--raised mdl-button--colored
        mdl-js-ripple-effect" type="submit">Enviar
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>