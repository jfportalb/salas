@extends('templates.dashboard')

@section('title','Recursos')

@section("css")
    <link rel="stylesheet" href="{!!asset('css/admin/listar.css')!!}">
    <link rel="stylesheet" href="{!!asset('css/user/alterar.css')!!}">
    <link rel="stylesheet" href="{!!asset('css/admin/local.css')!!}">

@endsection

@section('content')
    <div class="container">
        <div class="login-card">

            <div class="title-text center col-xs-12">
                <h2><b>Recursos</b></h2>
                <div class="clear"></div>
            </div>

            <div class="row">
                <div class="col-xs-offset-2 col-xs-8 col-sm-offset-4 col-sm-4">
                    <a data-toggle="modal" data-target="#criar_recurso_modal" class="button-area button-add button mdl-button mdl-js-button mdl-button--raised mdl-button--colored mdl-js-ripple-effect a-text" href="">
                        Adicionar Tipo de Recurso
                    </a>
                </div>
            </div>

            <div class="col-xs-12 mdl-data-table mdl-js-data-table mdl-shadow--2dp">
                <div class="s-dont row table-row">
                    <div class="col-sm-3">
                        <b>Nome</b>
                    </div>
                    <div class="col-sm-6">
                        <b>Descrição</b>
                    </div>
                    <div class="col-sm-3">
                        <div class="right">
                            <b>Ação</b>
                        </div>
                    </div>
                </div>
                @foreach($recursos as $recurso)

                    <div class="item-container row table-row-2 recurso" id="recurso_{{$recurso->id}}">
                        <div class="recurso-nome col-sm-3">
                            {{ $recurso->nome }}
                        </div>
                        <div class="recurso-descricao col-sm-6">
                            {{ $recurso->descricao }}
                        </div>
                        <div class="col-sm-3">
                            <div class="right">
                                <a data-item="Recurso" class="alterar table-button button mdl-button mdl-js-button
                        mdl-button--raised mdl-button--colored
                        mdl-js-ripple-effect a-text" href="{{ action('RecursosController@getAlterar', $recurso->id) }}">
                                    Alterar
                                </a>

                                <a class="deletar table-button button mdl-button mdl-js-button
                    mdl-button--raised mdl-button--colored mdl-js-ripple-effect a-text"
                                   href="{{route('removerTipoRecurso', ['id' => $recurso->id])}}">
                                    Deletar
                                </a>
                            </div>
                        </div>
                    </div>

                @endforeach
            </div>

            <div class="clear"></div>

        </div>
    </div>
@endsection

@section('popups')

    @include('includes.modals.confirm', ['item' => 'este recurso'])
    @include('templates.admin.create-recurso-modal')
    @include('templates.admin.recursos_modal')
    @include('includes.modals.alterar-item')
    @include('includes.modals.sucesso')

@endsection

@section('script')

    @if( Session::has("criar-recurso-fail") )
        <script>
            $('#criar_recurso_modal').modal('show');
        </script>
    @endif

    @if( Session::has("sucesso") )
        <script>
            $('#sucesso').modal('show');
        </script>
    @endif

    <script src="{{ asset('scripts/admin-recursos.js') }}"></script>
    <script src="{{ asset('scripts/remover-item.js') }}"></script>
    <script src="{{ asset('scripts/alterar-item.js') }}"></script>

@endsection