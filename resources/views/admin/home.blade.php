@extends('templates.dashboard')
@section('title','Início')

@section("css")
    <link rel="stylesheet" href="{!!asset('css/admin/home.css')!!}">
    <link rel="stylesheet" href="{!!asset('css/admin/pendencias.css')!!}">
@endsection

@section('content')

    <div class="container">
        <div class="login-card">


            <div class="title-text center col-xs-12">
                <h2><b>Início</b></h2>
                <div class="clear"></div>
            </div>

            <div class="col-sm-12">
                <div class="large-card mdl-card mdl-shadow--3dp">
                    <div class="mdl-card__title">
                        <h2>Bem vindo, {{ Auth::user()->nome }}!</h2>
                    </div>
                    <div class="mdl-card__supporting-text">
                        Esta é a sua área administrativa. Aqui você poderá controlar os pedidos de reserva de locais,
                        registro de usuários, entre outras coisas.
                    </div>
                </div>
            </div>

            <div class="col-sm-12 row">
                <div class="col-sm-12" >
                    <h3><b>Novos Usuários:</b> </h3>
                </div>
            </div>

            {!! csrf_field() !!}
            @foreach($users as $user)
                <div class="card-out-box col-md-4">
                    <div class="demo-card-square mdl-card mdl-shadow--2dp">
                        <div class="mdl-card__title mdl-card--expand">
                            {{$user->nome}}<br>
                            {{$user->email}}
                        </div>
                        <div class="mdl-card__supporting-text">
                            {{$user->celular}}<br>
                            Ramal: {{$user->ramal}} - Sala: {{$user->sala}}<br>
                        </div>
                        <div class="mdl-card__actions mdl-card--border">
                            <input type = "hidden" name = "email" value="{{ $user->email }}">
                            <div class="status-button button-div col-xs-4">
                                <a class="mdl-button mdl-button--colored mdl-js-button mdl-js-ripple-effect">
                                    Aceitar
                                    <input name="status" value="aceitar" style="display:none;"/>
                                </a>
                            </div>
                            <div class="status-button button-div col-xs-offset-4  col-xs-4">
                                <a class="right mdl-button mdl-js-button mdl-js-ripple-effect">
                                    Recusar
                                    <input name="status" value="reprovar" style="display:none;"/>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>

@endsection

@section('script')
    <script src="{{asset('scripts/pendencias-usuarios.js')}}"></script>
@endsection
