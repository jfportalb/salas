@extends('templates.dashboard')

@section('title','Pedidos')

@section("css")
    <link rel="stylesheet" href="{!!asset('css/admin/pendencias.css')!!}">
    <link rel='stylesheet' href='{{asset('libs/fullcalendar-2.6.0/fullcalendar.css')}}' />
    <link rel='stylesheet' href='{{asset('libs/mdl-selectfield/mdl-selectfield.min.css')}}' />
@endsection

@section('content')
    <div class="container">
        <div class="login-card">

            <h4>Ver calendário da sala:</h4>
            <div class="col-xs-8 mdl-selectfield mdl-js-selectfield mdl-js-textfield mdl-selectfield--floating-label">
                <select class="mdl-selectfield__select" name="local" id="local" required>
                    <option disabled selected>Escolha a sala</option>
                    @foreach($salas as $area)
                        <optgroup label="{{$area[0]->area->nome}}">
                            @foreach($area as $sala)
                                <option value="{{ $sala->id }}">
                                    {{$sala -> nome}}
                                </option>
                            @endforeach
                        </optgroup>
                    @endforeach
                </select>
                <label class="mdl-selectfield__label mdl-textfield__label" for="local">{{ucfirst("local")}}</label>
            </div>
            <div class="container calendario-div">
                <div class="col-sm-8 col-sm-offset-2">
                    <div id='calendar'></div>
                </div>
            </div>
            <hr>

            <div class="title-text center col-xs-12">
                <h2><b>Pedidos de Reserva</b></h2>
                <div class="clear"></div>
            </div>

            @include('includes.errors')
            @include('includes.status')

            {!! csrf_field() !!}
            @foreach($pedidos as $pedido)
                <div class="card-out-box col-md-4 pedido">
                    <input type = "hidden" value="{{ $pedido->id }}" name="pedido_id">
                    <div class="demo-card-square mdl-card mdl-shadow--2dp">
                        <div class="mdl-card__title mdl-card--expand">
                        </div>
                        <div class="mdl-card__supporting-text">
                            Evento: {{ $pedido->title }}<br>
                            Pedido por: {{ $pedido->user->nome }}<br>
                            Sala: {{$pedido->sala->nome}}<br>
                            Descrição: {{ $pedido->descricao }}<br>
                        </div>
                        <div class="mdl-card__actions mdl-card--border">
                            <input type="hidden" name="email" value="{{ $pedido->id }}">
                            <div class="status-button button-div col-xs-4">
                                <a class="mdl-button mdl-button--colored mdl-js-button mdl-js-ripple-effect">
                                    Aprovar
                                    <input name="status" value="aprovar" style="display:none;"/>
                                </a>
                            </div>
                            <div class="recusar button-div col-xs-offset-4  col-xs-4">
                                <a class=" right mdl-button mdl-js-button mdl-js-ripple-effect">
                                    Recusar
                                    <input name="status" value="recusar" style="display:none;"/>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach

        </div>
    </div>
@endsection

@section('script')
    <script src="{{asset("libs/fullcalendar-2.6.0/lib/moment.min.js")}}"></script>
    <script src="{{asset("libs/fullcalendar-2.6.0/fullcalendar.js")}}"></script>
    <script src='{{asset("libs/fullcalendar-2.6.0/lang/pt-br.js")}}'></script>
    <script src="{{asset("libs/fullcalendar-2.6.0/gcal.js")}}"></script>
    <script src="{{asset('libs/mdl-selectfield/mdl-selectfield.min.js')}}"></script>
    <script src="{{ asset('scripts/pendencias-reservas.js') }}"></script>
@endsection

@section('popups')
    @include('admin.modal-pedido')
    @include('admin.partials.confirm-recusa-pedido')
@endsection