@extends('templates.dashboard')

@section('title', 'Reserva de Salas - Cadastrar Usuário')

@section('css')
  <link rel="stylesheet" href="{!!asset('css/cadastro.css')!!}">
@endsection

@section('content')
  <div class="container">
    <div class="login-card white col-sm-offset-2 col-sm-8 mdl-shadow--2dp">

      <div class="center col-sm-12">
        <h3>Cadastrar Usuário</h3>
      </div>

      @include('includes.errors')
      @include('includes.error')
      @include('includes.status')

      <form action="" method="post">
        {{csrf_field()}}

        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label col-sm-12">
          <input class="mdl-textfield__input" type="text" name="nome" id="nome" value="{{ old('nome') }}"/>
          <label class="mdl-textfield__label" for="nome">Nome</label>
        </div>

        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label col-sm-6">
          <input class="mdl-textfield__input" type="text" name="email" id="email" value="{{ old('email') }}"/>
          <label class="mdl-textfield__label" for="email">E-mail</label>
        </div>

        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label col-sm-6">
          <input class="mdl-textfield__input" type="text" name="celular" id="celular" value="{{ old('celular') }}"/>
          <label class="mdl-textfield__label" for="celular">Celular</label>
        </div>

        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label col-sm-6">
          <input class="mdl-textfield__input" type="number" name="ramal" id="ramal" value="{{ old('ramal') }}"/>
          <label class="mdl-textfield__label" for="ramal">Ramal NCE</label>
        </div>

        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label col-sm-6">
          <input class="mdl-textfield__input" type="text" name="sala" id="sala" value="{{ old('sala') }}"/>
          <label class="mdl-textfield__label" for="sala">Sala</label>
        </div>

        <button class="login-button col-sm-offset-3 col-sm-6 mdl-button mdl-js-button mdl-button--raised mdl-button--colored mdl-js-ripple-effect" type="submit">
          Cadastrar
        </button>

      </form>
    </div>

  </div>
@endsection