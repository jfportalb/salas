$('.status-button').on('click', function (e) {
    e.preventDefault();

    var card = $(this).parents('.card-out-box'),
        email = $(this).siblings('input').first().val(),
        status = $(this).find('input').first().val(),
        token = $('input[name=_token]').val();

    card.fadeOut();

    $.ajax({
        type: 'post',
        url: window.location.href,
        data: {
            email: email,
            status: status,
            _token: token
        },
        success: function(response){
            if(!response.success){
                alert('Algum erro ocorreu... Tente novamente.');
                window.location.reload();
            }
        },
        error: function(jqXHR){
            alert('Algum erro ocorreu. Tente novamente.');
            window.location.reload();
        }
    });

    return false;
});