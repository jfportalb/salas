$(function () {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $('[data-toggle="tooltip"]').tooltip();

    $( ".button-add-local" ).click(function() {
        $( "#area_id" ).val(this.id);
    });

    var modalRecurso = $('#recurso');
    var areaRecursos = $('#recursos');
    var id = 0;

    $('.recurso').click(function(){
        var id = $(this).attr('id').substr(8);
        modalRecurso.find('#rec_id').val(id);
        modalRecurso.find('#recurso_nome').text($(this).find(".recurso-nome").text());
        modalRecurso.find('#recurso_descricao').text($(this).find(".recurso-descricao").text());
        $.ajax({
           url: "./getRecursos/"+id,
            success: function (result) {
                modalRecurso.modal('show');
                areaRecursos.empty();
                result.forEach(function(recurso){
                    areaRecursos.append("<p>"+recurso.codigo+"</p>");
                });
            }
        });

        return false;
    });
});
