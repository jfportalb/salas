$(function () {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $('[data-toggle="tooltip"]').tooltip();

    $( ".button-add-local" ).click(function() {
        $( "#area_id" ).val(this.id);
    });

    var modalLocal = $('#local');

    $('.sala').click(function(){
        var id = $(this).attr('id').substr(5);
        $.ajax({
            type: 'post',
            data: {id: id},
            success: function (result) {
                modalLocal.find('#sala_nome').text(result.nome);
                modalLocal.find('#sala_area').text(result.area.nome);
                modalLocal.find('#sala_capacidade').text(result.capacidade);
                modalLocal.find('#sala_descricao').text(result.descricao);
                modalLocal.modal('show');
                modalLocal.find('#sala_recursos').empty();
                result.recursos.forEach(function(recurso){
                    modalLocal.find('#sala_recursos').append("<p>"+recurso.tipo_recurso.nome+" - "+recurso.codigo+"</p>");
                });
                modalLocal.find('#sala_adms').empty();
                result.area.administradores.forEach(function(admin){
                    modalLocal.find('#sala_adms').append("<p>"+admin.nome+"</p>");
                });
            }
        });
    });
});