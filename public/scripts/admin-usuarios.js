$(function () {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    function showAlterarSenhaModal() {
        $('#senha').modal('show');
    }

    var modalUser = $('#user');

    $('.user').click(function(){
        var id = $(this).attr('id').substr(5);
        $.ajax({
            url: './get-usuario/'+id,
            type: 'post',
            success: function (result) {
                modalUser.find('#user_nome').text(result.nome);
                modalUser.find('#user_email').text(result.email);
                modalUser.find('#user_sala').text(result.sala);
                modalUser.find('#user_ramal').text(result.ramal);
                modalUser.find('#user_celular').text(result.celular);
                modalUser.modal('show');
            }
        });
    });
});