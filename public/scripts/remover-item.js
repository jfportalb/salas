$('.deletar').on('click', function(e){
    e.preventDefault();


    if(this.hasAttribute('data-item')){
        $('.modal-body', '#confirmacao ').text('Deseja mesmo deletar '+$(this).attr('data-item')+'?');
    }

    var url = $(this).attr('href'),
        item = $(this).parents('.item-container');

    $('#confirmacao').modal('show');

    $('#aprovar').one('click', function () {
        $.ajax({
            method: 'get',
            url: url,
            success: function (response) {
                if(response.success){
                    item.fadeOut();
                }
                else{
                    location.href = url;
                }
            },
            error: function() {
                location.href = url;
            }
        });
    });

    return false;
});

