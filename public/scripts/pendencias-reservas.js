var modalPedido = $('#pedido_modal'),
    calendar = $('#calendar'),
    localSelect = $('#local');

$('.status-button').on('click', function (e) {
    e.preventDefault();

    var card = $(this).parents('.card-out-box'),
        reserva = $(this).siblings('input').first().val(),
        status = $(this).find('input').first().val(),
        token = $('input[name=_token]').val();

    card.fadeOut();

    $.ajax({
        type: 'post',
        url: window.location.href,
        data: {
            pedido_id: reserva,
            status: status,
            _token: token
        },
        success: function (response) {
            if (!response.success) {
                alert('Algum erro ocorreu... Tente novamente.');
                window.location.reload();
            }
            if (response.confirm) {
                card.fadeIn();
                $('.modal-title', modalPedido).text('Confirmar o pedido ' + response.nome);
                $('.modal-body', modalPedido).html(response.pedidos);
                modalPedido.modal('show');
            }
        },
        error: function (jqXHR) {
            alert('Algum erro ocorreu. Tente novamente.');
            window.location.reload();
        }
    });

    return false;
});

$('.pedido').on('click', function () {
    var pedidoId = $('input[name=pedido_id]', this).val();

    $.ajax({
        url: window.location.href + '/pedido/' + pedidoId,
        method: 'get',
        success: function (response) {

            if (!response.success) {
                alert('Algum erro ocorreu... Tente novamente.');
                window.location.reload();
            }
            else {
                $('.modal-title', modalPedido).text('Pedido: ' + response.nome);
                $('.modal-body', modalPedido).html(response.pedido);
                modalPedido.modal('show');
            }
        },
        error: function (jqXHR) {
            alert('Algum erro ocorreu. Tente novamente.');
            window.location.reload();
        }
    });
});

$(document).on('click', '#aprovar-confirmed', function () {
    var reserva = $(this).siblings('input').first().val(),
        token = $('input[name=_token]').val();
    console.log(reserva);
    console.log(token);
    $.ajax({
        type: 'post',
        url: window.location.href,
        data: {
            pedido_id: reserva,
            status: "aprovar",
            confirma_aprovar: "true",
            _token: token
        },
        success: function (response) {
            if (!response.success) {
                alert('Algum erro ocorreu... Tente novamente.');
                window.location.reload();
            } else
                window.location.reload();
        },
        error: function (jqXHR) {
            alert('Algum erro ocorreu. Tente novamente.');
            window.location.reload();
        }
    });
});

$('.recusar').on('click', function (e) {
    e.preventDefault();

    var card = $(this).parents('.card-out-box'),
        reserva = $(this).siblings('input').first().val(),
        token = $('input[name=_token]').val();

    if (this.hasAttribute('data-item')) {
        $('.modal-body', '#confirmacao ').text('Deseja mesmo deletar ' + $(this).attr('data-item') + '?');
    }


    $('#recusa').modal('show');

    $('#recusar').one('submit', function (e) {
        e.preventDefault();
        $('#recusa').modal('hide');

        var message = $(this).find('#reason').val();
        $.ajax({
            type: 'post',
            url: window.location.href,
            data: {
                pedido_id: reserva,
                status: 'recusar',
                message: message,
                _token: token
            },
            success: function (response) {
                if (!response.success) {
                    alert('Algum erro ocorreu... Tente novamente.');
                    window.location.reload();
                } else
                    card.fadeOut();
            },
            error: function (jqXHR) {
                alert('Algum erro ocorreu. Tente novamente.');
                window.location.reload();
            }
        });
    });

    return false;
});

//Abre o calendar
localSelect.one('change', function (event) {
    calendar.fullCalendar({
        header: {
            left: 'prev,next today',
            center: 'title',
            right: 'month,agendaWeek,agendaDay'
        },
        selectable: false,
        selectHelper: true,
        editable: false,
        eventLimit: true,
        eventBackgroundColor: "#AC1008",
        eventBorderColor: 'darkred',
        timezone: 'local',
        dayClick: function (date, jsEvent, view) {
            setTimeout(function () {
                calendar.fullCalendar('changeView', 'agendaDay');
                if (view.name === "month") {
                    calendar.fullCalendar('gotoDate', date);
                    calendar.fullCalendar('changeView', 'agendaDay');
                }
            }, 100);
        }
    });
});

//Altera os eventos caso mude de sala.
localSelect.on('change', function (event) {
    var id = $(this).val();
    atualizaCalendar(id);
});

function atualizaCalendar(id) {
    calendar.fullCalendar('removeEvents');
    $.ajax({
        url: '../reservas/' + id,
        type: 'GET',
        success: function (response) {
            calendar.fullCalendar('addEventSource', {
                events: response,
                editable: false
            });
            reservasInput.empty();
            reservasFeitas = 0;
        },
        error: function () {
            alert('Algum erro ocorreu. Por favor tente novamente.');
            window.location.reload();
        }
    });
}