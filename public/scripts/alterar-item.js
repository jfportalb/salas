$('.alterar').on('click', function(e){
    e.preventDefault();

    var url = $(this).attr('href'),
        itemName = $(this).attr('data-item');

    $.ajax({
        method: 'get',
        url: url,
        success: function(response){
            if(response.success){
                var alterarModal = $('#alterar_modal'),
                    modalTitle = $('.modal-title', alterarModal),
                    titleNewId = 'Alterar'+itemName+'Modal';

                alterarModal.attr('aria-labelledby', titleNewId);
                modalTitle.attr('id', titleNewId);
                modalTitle.text('Alterar '+itemName);

                $('.modal-body', alterarModal).html(response.formulario);
                alterarModal.modal('show');

                // Atualiza os componentes do material design lite
                componentHandler.upgradeDom();

                $('form', alterarModal).bind('submit', form_submit);
            }
            else{
                window.location.href = url;
            }
        },
        error: function(){
            window.location.href = url;
        }
    });

    return false;
});

var form_submit = function(event){
    event.preventDefault();

    var form = $(this),
        action = form.attr('action'),
        data;

    form.append();

    $.ajax({
        method: 'post',
        url: action,
        data: data,
        success: function(response){
            var mensagem = response.sucesso,
                modal_sucesso = $('#sucesso');

            form.find('.nce-red').remove();
            $('#alterar_modal').modal('toggle');
            $('.modal-body', modal_sucesso).text(mensagem);
            modal_sucesso.modal('show');
        },
        error: function(jqXHR){
            var errors = JSON.parse(jqXHR.responseText);

            // Remove todas as mensagens de erro que podem estar no formulário identificadas pela classe nce-red
            form.find('.nce-red').remove();

            $.each(errors, function(i, el){
                $.each(el, function(i, inner_el){
                    var mensagem = $('<div/>', {'class':'col-sm-12 nce-red mdl-shadow--2dp'}).append(
                        $('<p/>', {'class':'mdl-color-text--white mdl-typography--text-center mdl-cell mdl-cell--12-col'})
                            .text(inner_el)
                    );
                    form.prepend(mensagem);
                });
            });
        }
    });
};

