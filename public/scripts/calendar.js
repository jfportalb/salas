var n = 0,
    reservasFeitas = 0,
    eventosRemovidos = 0,
    calendar = $('#calendar'),
    localSelect = $('#local'),
    reservasInput = $('#reservas'),
    eventosAtuais = [],
    modal_sucesso = $('#sucesso');

function writePedido(event) {
    var horarios = pedidoHorarios(event);
    reservasInput.append("<div class = 'col-sm-12 nce-red mdl-shadow--2dp reserva-box'>" +
        "<p class='mdl-color-text--white mdl-typography--text-center mdl-cell mdl-cell--12-col' id='event-" + event.id + "'>" +
        pedidoText(horarios) + "</p>" +
        "<button type='button' id='remove-" + event.id + "' name='remove' >Remover</button></div>");
    reservasFeitas++;
}
function overwritePedido(event) {
    eventosAtuais[event.id].start = event.start;
    eventosAtuais[event.id].end = event.end;
    var horarios = pedidoHorarios(event),
        evento = $("#event-" + event.id);
    evento.text(pedidoText(horarios));
}
function pedidoText(horarios) {
    return horarios[0] +" -> "+ horarios[1];
}
function pedidoHorarios(event) {
    var start = new Date(event.start);
    var startDate = $.format.date(start, "yyyy-MM-dd HH:mm:ss");
    var end = new Date(event.end);
    var endDate = $.format.date(end, "yyyy-MM-dd HH:mm:ss");
    return [startDate, endDate];
}

//Abre o calendar
localSelect.one('change', function (event) {
    $("#pedidos-input").show();
    calendar.fullCalendar({
        header: {
            left: 'prev,next today',
            center: 'title',
            right: 'month,agendaWeek,agendaDay'
        },
        selectable: true,
        selectHelper: true,
        editable: true,
        eventLimit: true,
        eventBackgroundColor: "#AC1008",
        eventBorderColor: 'darkred',
        timezone: 'local',
        select: function (start, end) {
            if (calendar.fullCalendar('getView').name == 'agendaDay' || calendar.fullCalendar('getView').name === "agendaWeek") {
                var eventData;
                eventData = {
                    id: n++,
                    start: start,
                    end: end
                };
                writePedido(eventData);
                calendar.fullCalendar('renderEvent', eventData, true);
                eventosAtuais.push(eventData);
                calendar.fullCalendar('unselect');
            } else {
                calendar.fullCalendar('unselect');
            }
        },
        eventDrop: function (event) {
            overwritePedido(event);
        },
        eventResize: function (event) {
            overwritePedido(event);
        },
        eventOverlap: function (stillEvent) {
            if (stillEvent.hasOwnProperty("overlap"))
                return stillEvent.overlap;
            else
                return false;
        },
        selectOverlap: function (event) {
            if (event.hasOwnProperty("overlap"))
                return event.overlap;
            else
                return false;
        },
        dayClick: function (date, jsEvent, view) {
            setTimeout(function () {
                calendar.fullCalendar('changeView', 'agendaDay');
                if (view.name === "month") {
                    calendar.fullCalendar('gotoDate', date);
                    calendar.fullCalendar('changeView', 'agendaDay');
                }
            }, 100);
        }
    });
});

//Altera os eventos caso mude de sala.
localSelect.on('change', function (event) {
    var id = $(this).val();
    var local = $(this).data('lastValue', $(this).val()); //TODO N está funcionando
    if (reservasFeitas != 0) {
        $('#confirmacao').modal('show');
        $('#cancelar').one('click', function () {
        });
        $('#aprovar').one('click', function () {
            atualizaCalendar(id);
        });
    } else
        atualizaCalendar(id);
});

function atualizaCalendar(id) {
    calendar.fullCalendar('removeEvents');
    $.ajax({
        url: '../reservas/' + id,
        type: 'GET',
        success: function(response){
            calendar.fullCalendar('addEventSource', {
                events: response,
                editable: false
            });
            reservasInput.empty();
            reservasFeitas = 0;
        },
        error: function () {
            alert('Algum erro ocorreu. Por favor tente novamente.');
            window.location.reload();
        }
    });
}

$('#pedido-form').on('submit', function (event) {
    event.preventDefault();

    var form = $(this),
        action = form.attr('action');

    var pedido = {
        local: $('select', form).val(),
        titulo: $('input[name=titulo]', form).val(),
        inicio: $('input[name=inicio]', form).val(),
        fim: $('input[name=fim]', form).val(),
        descricao: $('textarea', form).val()
    };

    var reservas = [];
    eventosAtuais.forEach(function(event){
        var horario = pedidoHorarios(event);
        reservas.push({
            start: horario[0],
            end: horario[1]
        });
    });

    $.ajax({
        method: 'post',
        url: action,
        data: {
            _token: $('input[name=_token]').val(),
            pedido: pedido,
            reservas: reservas
        },
        success: function (response) {
            //Atualizando os eventos no calendar
            eventosAtuais.forEach(function(event){
                event.title = pedido.titulo;
                event.color = '#5E0000';
                event.overlap = true;
                event.editable = false;
                calendar.fullCalendar('removeEvents', event.id);
                calendar.fullCalendar('renderEvent', event, true);
            });

            //Limpando o form
            form.find('.nce-red').remove();
            form.find('input').val("");
            form.find('textarea').val("");
            eventosAtuais = [];
            reservasFeitas = 0;

            $('.modal-body', modal_sucesso).text(response.msg);
            modal_sucesso.modal('show');
        },
        error: function (jqXHR, textStatus) {
            var errors = JSON.parse(jqXHR.responseText);
            // Remove todas as mensagens de erro que podem estar no formulário identificadas pela classe nce-red
            form.find('.nce-red').remove();

            $.each(errors, function (i, el) {
                $.each(el, function (i, inner_el) {
                    var mensagem = $('<div/>', {'class': 'col-sm-12 nce-red mdl-shadow--2dp'}).append(
                        $('<p/>', {'class': 'mdl-color-text--white mdl-typography--text-center mdl-cell mdl-cell--12-col'})
                            .text(inner_el)
                    );
                    form.prepend(mensagem);
                });
            });
        }
    });

    return false;
});

$(document).on('click', 'button[name=remove]', function () {
    var id = $(this).attr('id').substr(7);
    calendar.fullCalendar('removeEvents', id);
    $(this).parent().remove();
    eventosAtuais.splice(id - eventosRemovidos++, 1);
    reservasFeitas--;
});