var modalPedido = $('#pedido_modal');

$('.pedido').on('click', function () {
    var pedidoId = $('input[name=pedido-id]', this).val();

    $.ajax({
        url: window.location.href + "/" + pedidoId,
        method: 'get',
        success: function (response) {

            if (!response.success) {
                alert('Algum erro ocorreu... Tente novamente.');
                window.location.reload();
            }
            else {
                $('.modal-title', modalPedido).text('Pedido: ' + response.nome);
                $('.modal-body', modalPedido).html(response.pedido);
                modalPedido.modal('show');
            }
        },
        error: function (jqXHR) {
            alert('Algum erro ocorreu. Tente novamente.');
            window.location.reload();
        }
    });
});