<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Area extends Model
{
	protected $table = 'areas';

	protected $fillable = ['nome'];

	public function salas()
	{
		return $this->hasMany('App\Sala');
	}

    public function administradores()
    {
        return $this->belongsToMany('App\User', 'areas_users');
    }
}
