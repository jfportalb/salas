<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sala extends Model
{
    protected $table = 'salas';

    protected $fillable = ['nome', 'capacidade', 'descricao', 'area_id'];

    public function pedidos()
    {
        return $this->hasMany('App\Pedido');
    }

    public function recursos()
    {
        return $this->hasMany('App\Recurso');
    }

    public function area()
    {
        return $this->belongsTo('App\Area');
    }

    public function getReservas(\Closure $filter = null)
    {
        $reservas = [];
        foreach ($this->pedidos as $pedido)
            if ($filter == null)
                $reservas = array_merge($reservas, $pedido->reservas->toArray());
            else
                $reservas = array_merge($reservas, $pedido->reservas->filter(function ($reserva) use ($filter) {
                    return $filter($reserva);
                })->toArray());
        return $reservas;
    }
}
