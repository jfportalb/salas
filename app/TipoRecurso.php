<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TipoRecurso extends Model
{
    protected $table = 'tipo_recursos';

    protected $fillable = ['nome', 'descricao'];

    public function recursos()
    {
        return $this->hasMany('App\Recurso');
    }
}
