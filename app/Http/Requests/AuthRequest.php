<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

abstract class AuthRequest extends Request
{

    public function loginValidator()
    {
        return [
            'email' => 'required|email',
            'password' => 'required'
        ];
    }

}
