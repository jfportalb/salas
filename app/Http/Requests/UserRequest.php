<?php

namespace App\Http\Requests;


class UserRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nome' => 'required|max:255',
            'email' => 'required|email|max:255',
            'celular' => 'required|min:8|max:15',
            'ramal' => 'required|numeric|min:0000|max:9999',
            'sala' => 'required|max:255',
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'nome.required' => 'O campo "Nome" é obrigatório',
            'nome.max:255' => 'O nome deve possuir no máximo 255 caracteres',
            'email.required' => 'O campo "Email" é obrigatório',
            'email.email' => 'O email inserido não é válido',
            'email.max:255' => 'O email deve possuir no máximo 255 caracteres',
            'password.required' => 'O campo "Senha" é obrigatório',
            'password.confirmed' => 'As senhas inseridas não são iguais',
            'password.min:6' => 'A senha deve possuir no mínimo 6 caracteres',
            'celular.required' => 'O campo "Celular" é obrigatório',
            'celular.min:8' => 'O celular deve possuir no mínimo 8 caracteres',
            'celular.max:15' => 'O celular deve possuir no máximo 15 caracteres',
            'ramal.required' => 'O campo "Ramal NCE" é obrigatório',
            'ramal.numeric' => 'O campo "Ramal NCE" deve ser númerico',
            'ramal.min:0000' => 'O ramal deve possuir no mínimo 4 caracteres',
            'ramal.max:9999' => 'O ramal deve possuir no máximo 8 caracteres',
            'area.required' => 'O campo "Área de atuação" é obrigatório',
            'area.max:255' => 'A área de atuação deve possuir no máximo 255 caracteres',
            'atuacao.required' => 'O campo "Atuação no NCE" é obrigatório',
            'atuacao.max:255' => 'A atuação no NCE deve possuir no máximo 255 caracteres',
            'sala.required' => 'O campo "Sala" é obrigatório',
            'sala.max:255' => 'A sala deve possuir no máximo 255 caracteres',
            'nce_id.required' => 'O campo "Número identificador" é obrigatório',
            'nce_id.numeric' => 'O campo "Número identificador" deve ser númerico'
        ];
    }
}
