<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Pedido;
use App\Reserva;
use App\Sala;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Validator;

class ReservasController extends Controller
{
    public function getReservasFromSala($sala)
    {
        $sala = Sala::findORFail($sala);
        return $sala->getReservas();
    }

    public function getPedidosFromSala($sala)
    {
        $user = Auth::user();
        $sala = Sala::findORFail($sala);
        if (($user->isNormalAdmin() && $user->areas->contains('id', $sala->area_id)) || $user->isSuperAdmin()) {
            return $sala->pedidos;
        } else
            return $user->pedidos->where('sala_id', $sala->id);
    }

    public function putPedido(Request $request)
    {
        $this->validate($request, [
            'pedido.local' => 'required|exists:salas,id',
            'pedido.titulo' => 'required',
            'pedido.descricao' => 'required',
        ]);
        if ($request->reservas == null)
            return response("Sem reservas", 404); //TODO Botar um erro apropriado
        $user = Auth::user();
        $reservas = [];
        $pedidos = Pedido::where('sala_id', $request->pedido['local'])->where('status_aprovacao', Pedido::STATUS_ACEITO)->get();
        foreach ($request->input('reservas') as $reserva) {
            $validator = Validator::make($reserva, [
                'start' => 'required',
                'end' => 'required|min:' . $request->start,
            ]);
            if ($validator->fails()) {
                return response("Validação da reserva falhou", 404); //TODO Botar um erro apropriado
            }
            foreach ($pedidos as $pedido) {
                if (!$pedido->reservas->filter(function ($value) use ($reserva) {
                        return $value->start > $reserva['start'] && $value->start < $reserva['end'];
                    })->isEmpty() ||
                    !$pedido->reservas->filter(function ($value) use ($reserva) {
                        return $value->start < $reserva['start'] && $value->end > $reserva['start'];
                    })->isEmpty()
                ) {
                    return response("Concorrência =/", 404); //TODO Botar um erro apropriado
                }
            }
            array_push($reservas, new Reserva($reserva));
        }
        $pedido = new Pedido([
            'title' => $request->input("pedido.titulo"),
            'descricao' => $request->input("pedido.descricao"),
            'user_id' => $user->id,
            'sala_id' => $request->input("pedido.local"),
        ]);
        $pedido->save();
        $pedido->reservas()->saveMany($reservas);
        return response()->json(['success' => true, 'msg' => "Seu pedido foi registrado. Por favor, aguarde sua avaliação."]);
    }

    /**
     * Mostra ao administrador as reservas pendentes no sistema
     *
     * @return $this
     */
    public function getPendenciasReserva()
    {
        $user = Auth::user();
        if ($user->isSuperAdmin())
            $data['pedidos'] = Pedido::where('status_aprovacao', Pedido::STATUS_PENDENTE)->get();
        elseif ($user->isNormalAdmin())
            $data['pedidos'] = Pedido::where('status_aprovacao', Pedido::STATUS_PENDENTE)->get()
                ->filter(function ($pedido) use ($user) {
                    foreach ($user->areas as $area)
                        if ($area->salas->contains($pedido->sala))
                            return true;
                    return false;
                });
        else
            return back(403);

        $data['salas']= Sala::all()->groupBy('area_id');
        return view('admin.pendencias')->with($data);
    }

    public function getVisualizarPedido($id)
    {
        $pedido = Pedido::findOrFail($id);
        $usuario = Auth::user();

        if ($pedido->user_id == $usuario->id ||
            ($usuario->isNormalAdmin() && $usuario->areas->contains('id', $pedido->sala->area_id)) ||
            $usuario->isSuperAdmin()
        ) {
            return response()->json([
                'success' => true,
                'nome' => $pedido->title,
                'pedido' => view('admin.partials.visualizar-pedido')->with
                ('pedido', $pedido)->render()
            ]);
        } else
            abort(403);
    }

    /**
     * Processa a aprovação/reprovação de um pedido de reserva
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function postAvaliarReserva(Request $request)
    {
        $this->validate($request, [
            'pedido_id' => 'required',
            'status' => 'required' // se foi aprovado ou não
        ]);

        $pedido = Pedido::findOrFail($request->pedido_id);

        if ($pedido->status_aprovacao != Pedido::STATUS_PENDENTE)
            //TODO Definir o retorno.
            return response()->json(['msg' => "Esse pedido já foi aceito/recusado ou não existe mais."]);

        if ($request->status == 'aprovar') {
            $pedidosConcorrentes = [];
            //Analisa os pedidos que batem com o horário
            $pedidos = $pedido->sala->pedidos->filter(function ($pedido) {
                return $pedido->status_aprovacao == Pedido::STATUS_PENDENTE;
            });
            foreach ($pedidos as $other) {
                foreach ($pedido->reservas as $reserva) {
                    if (!$other->reservas->filter(function ($value) use ($reserva) {
                            return $value->start > $reserva->start && $value->start < $reserva->end;
                        })->isEmpty() ||
                        !$other->reservas->filter(function ($value) use ($reserva) {
                            return $value->start < $reserva->start && $value->end > $reserva->start;
                        })->isEmpty()
                    ) {
                        array_push($pedidosConcorrentes, $other);
                        break;
                    }
                }
            }

            if ($request->confirma_aprovar || empty($pedidosConcorrentes)) {
                foreach ($pedidosConcorrentes as $concorrente)
                    $this->reprovaPedido($concorrente, "Seu pedido foi recusado pois havia outro pedido
                    no mesmo horário que foi escolhido");

                $this->aprovaPedido($pedido);
                return response()->json(['success' => true]);
            } else {
                //TODO retornar mensagem com pedidos concorrentes e pedir para confirmar.
                return response()->json([
                    'confirm' => true,
                    'success' => true,
                    'nome' => $pedido->title,
                    'pedidos' => view('admin.partials.confirmar-pedido')->with
                    ('pedidos', $pedidosConcorrentes)->with('pedido_id', $pedido->id)->render(),
                ]);
            }
        } else {
            $this->validate($request, [
                'message' => 'required',
            ]);
            $this->reprovaPedido($pedido, $request->message);
            return response()->json(['success' => true]);
        }
    }

    private function reprovaPedido($pedido, $m)
    {
        $usuario = $pedido->user;
        $email = $usuario->email;
//        Mail::send('emails.reserve-reproved', [
//            'usuario' => $usuario,
//            'pedido' => $pedido,
//        ], function ($message) use ($email) {
//            $message->to($email)->subject('Reserva Recusada no Sistema');
//        });

        $pedido->reject();
    }

    private function aprovaPedido($pedido)
    {
        $usuario = $pedido->user;
        $email = $usuario->email;
//        Mail::send('emails.reserve-aproved', [
//            'usuario' => $usuario,
//            'pedido' => $pedido,
//        ], function ($message) use ($email) {
//            $message->to($email)
//                ->subject('Reserva Aprovada no Sistema');
//        });

        $pedido->accept();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $reserva = Reserva::findOrFail($id);
        $usuario = Auth::user();

        if (!$usuario->isAdmin()) {
            if ($reserva->user_id == $usuario->id) {
                // Deletar ou marcar como reprovado?
                $reserva->delete();
            } else {
                abort(403);
            }
        } elseif ($usuario->isNormalAdmin()) {
            if ($usuario->areas->contains('id', $id)) {
                // Deletar ou marcar como reprovado? // manda email dizendo algo??
                $reserva->delete();
            } else {
                abort(403); // Admin não é da área referente a reserva: Forbiden
            }
        } elseif ($usuario->isSuperAdmin()) {
            // Só aceita.
            $reserva->delete();
        } else {
            abort(403); // Usuário com permissões desconhecidas: Forbiden
        }
    }

    /**
     * Lista os pedidos do usuário
     *
     * @return \Illuminate\Http\Response
     */
    public function getUserPedidos()
    {
        $user = Auth::user();
        $pedidos = Pedido::where('user_id', $user->id)->get()->groupBy("status_aprovacao");
        return view('user.pedidos')->with('pedidos', $pedidos);
    }

    /**
     * Cancela o pedido $id do usuário logado
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function getCancelarPedido($id)
    {
        $pedido = Pedido::findOrFail($id);
        $usuario = Auth::user();

        if ($pedido->user_id == $usuario->id) {
            $pedido->delete();
            return response()->json(["success" => true, "msg" => "O pedido foi cancelado com sucesso."]);
        } else {
            abort(403);
        }
    }
}
