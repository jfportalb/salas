<?php

namespace App\Http\Controllers;

use App\Recurso;
use App\TipoRecurso;
use Illuminate\Http\Request;
use Validator;

class RecursosController extends Controller
{

    public function getRecursos($id)
    {
        $tipo = TipoRecurso::findOrFail($id);

        return $tipo->recursos;
    }

    public function getTipoRecursos()
    {
        $recursos = TipoRecurso::all();

        return view('admin.recursos', compact('recursos'));
    }

    public function getDeletar($id)
    {
        $recurso = Recurso::findOrFail($id);

        $recurso->delete();

        if(request()->ajax()){
            return response()->json(['success' => true]);
        }

        return redirect()->back()->with('sucesso', 'O recurso foi deletado com sucesso!');
    }

    public function getDeletarTipo($id)
    {
        $tipo = TipoRecurso::findOrFail($id);

        $tipo->delete();

        if(request()->ajax()){
            return response()->json(['success' => true]);
        }

        return redirect()->back()->with('sucesso', 'O recurso foi deletado com sucesso!');
    }

    public function getAlterar($id)
    {
        $recurso = TipoRecurso::findOrFail($id);

        if(request()->ajax()){
            return response()->json([
                'success' => true,
                'formulario' => view('recurso.partials.alterar-form')->with('recurso', $recurso)->render()
            ]);
        }

        return view('recurso.alterar')->with(compact('recurso'));
    }

    public function postAlterar(Request $request, $id)
    {
        $this->validate($request, [
            'nome' => 'required'
        ]);

        $recurso = TipoRecurso::findOrFail($id);

        $recurso->nome = $request->nome;
        $recurso->descricao = $request->descricao;
        $recurso->save();

        if(request()->ajax()){
            return response()->json(['sucesso' => 'Recurso alterado com sucesso!']);
        }

        return back()->with('sucesso', 'Recurso alterado com sucesso!');
    }

    public function postCriarRecurso(Request $request){

        $validator = Validator::make($request->all(), [
            'codigo' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect('admin/recursos')
                ->withErrors($validator)
                ->withInput()
                ->with('criar-recurso-fail',true);
        }else{
            $tipo = TipoRecurso::findOrFail($request->id);
            $tipo->recursos()->create($request->all());
            return redirect('admin/recursos');
        }

    }

    public function postCriarTipoRecurso(Request $request){

        $validator = Validator::make($request->all(), [
            'nome' => 'required',
            'descricao' => '',
        ]);

        if ($validator->fails()) {
            return redirect('admin/recursos')
                ->withErrors($validator)
                ->withInput()
                ->with('criar-recurso-fail',true);
        }else{
            TipoRecurso::create($request->all());
            return redirect('admin/recursos');
        }

    }
}
