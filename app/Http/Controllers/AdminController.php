<?php

namespace App\Http\Controllers;

use App\Sala;
use App\TipoRecurso;
use App\User;
use App\Area;
use App\Recurso;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Mail;
use Validator;

class AdminController extends Controller
{

    /**
     * Mostra a página principal da área adminstrativa
     *
     * @return \Illuminate\View\View
     */
    public function getHome()
    {
        $users = User::where('status_login', 0)->get();

        return view("admin.home")->with('users',$users);
    }

    /**
     * Executa a alteração de aprovação ou reprovação de uma pendência
     *
     * @param Request $request
     * @return \Illuminate\View\View
     */
    public function postAlterarPendenciaUsuario(Request $request)
    {
        if ($request->ajax()) {
            $this->validate($request, [
                'status' => 'required',
                'email' => 'required | email'
            ]);

            $usuario = User::where('email', $request->email)->first();

            if ( ($request->status == 'aceitar') && (!is_null($usuario)) ) {
                // Envia email falando que o cadastro foi aprovado
                $email = $request->email;
                Mail::send('emails.register-aproved', [
                        'nome'  => $usuario->nome,
                        'email' => $usuario->email,
                ], function ($message) use ($email) {
                    $message->to($email)
                            ->subject('Cadastro Aprovado no Sistema');
                });

                $usuario->status_login = User::STATUS_ACTIVE;
                $usuario->save();

                return response()->json(['success' => true]);
            }
            else {
                // Envia email falando que o cadastro foi reprovado
                $email = $request->email;
                Mail::send('emails.register-reproved', [
                        'nome'  => $usuario->nome,
                        'email' => $usuario->email,
                ], function ($message) use ($email) {
                    $message->to($email)
                            ->subject('Cadastro Reprovado no Sistema');
                });

                $usuario->status_login = User::STATUS_REPROVED;
                $usuario->save();

                return response()->json(['success' => true]);
            }
        }
        else {
            return back();
        }
    }

    public function getLocais()
    {
        $areas = Area::get();

        return view('admin.locais', compact('areas'));
    }

    public function getLocal(Request $request)
    {
        $sala = Sala::where('id', $request->id)->with('area', 'area.administradores', 'recursos', 'recursos.tipoRecurso')->first();
        $sala->setVisible(['nome', 'capacidade', 'descricao', 'area', 'recursos']);
        $sala->area->setVisible(['nome', 'administradores']);
        foreach ($sala->area->administradores as $admin) {
            $admin->setVisible(['nome']);
        }
        foreach ($sala->recursos as $recurso) {
            $recurso->setVisible(['codigo', 'tipoRecurso']);
        }

        return $sala;
    }

    public function getAlterarLocal($id)
    {
        $local = Sala::findOrFail($id);

        if(request()->ajax()){
            return response()->json([
                'success' => true,
                'formulario' => view('sala.partials.alterar-form')->with('local', $local)->render()
            ]);
        }

        return view('sala.alterar')->with(compact('local'));
    }

    public function postAlterarLocal(Request $request, $id)
    {
        $local = Sala::findOrFail($id);

        $this->validate($request, [
            'nome' => 'required',
            'capacidade' => 'required|numeric',
        ]);

        $local->update($request->except('_token'));
        $local->save();

        if(request()->ajax()){
            return response()->json(['sucesso' => 'O local foi alterado com sucesso!']);
        }

        return redirect(action('AdminController@getLocais'))->with('sucesso', 'O local foi alterado com sucesso!');
    }

    public function getDeletarLocal($id)
    {
        $local = Sala::findOrFail($id);

        $local->delete();

        if(request()->ajax()){
            return response()->json(['success' => true]);
        }

        return redirect()->back()->with('sucesso', 'O local foi deletado com sucesso!');
    }

    public function getDeletarArea($id)
    {
        $area = Area::findOrFail($id);

        if(Auth::user()->isSuperAdmin()){
            foreach($area->salas as $sala){
                $sala->delete();
            }
            $area->delete();

            if(request()->ajax()){
                return response()->json(['success' => true]);
            }

            return redirect()->back()->with('sucesso', 'A área e seus respectivos locais foram deletados com sucesso!');
        }
        else{
           abort(404);
        }
    }

    public function getCadastrar()
    {
        return view('admin.cadastrar-admin');
    }

    public function postCadastrar(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email|max:255|unique:users',
            'nome' => 'required|max:255',
            'celular' => 'required|max:15',
            'ramal' => 'required|numeric',
            'sala' => 'required|max:255',
        ]);

        if ($validator->fails()){
            return back()->with('create_admin_failed', true)->withErrors($validator)->withInput();
        }

        $user = User::create($request->all());
        $user->status_login = User::STATUS_ACTIVE;
        $user->tipo_login = User::TYPE_ADMIN;
        $user->save();

        $email = $user->email;
        Mail::send('emails.registered', [
            'nome'  => $user->nome,
            'email' => $user->email,
        ], function ($message) use ($email) {
            $message->to($email)
                ->subject('Cadastro criado no Sistema');
        });

        return redirect(action('UserController@getListarAdmins'))
            ->with('sucesso', 'Administrador cadastrado com sucesso!');

    }

    public function postCadastrarSuper(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email|max:255|unique:users',
            'nome' => 'required|max:255',
            'celular' => 'required|max:15',
            'ramal' => 'required|numeric',
            'sala' => 'required|max:255',
        ]);

        if ($validator->fails()){
            return back()->with('create_SuperAdmin_failed', true)->withErrors($validator)->withInput();
        }

        $user = User::create($request->all());
        $user->status_login = User::STATUS_ACTIVE;
        $user->tipo_login = User::TYPE_SUPERADMIN;
        $user->save();

        $email = $user->email;
        Mail::send('emails.registered', [
            'nome'  => $user->nome,
            'email' => $user->email,
        ], function ($message) use ($email) {
            $message->to($email)
                ->subject('Cadastro criado no Sistema');
        });

        return redirect(action('UserController@getListarAdmins'))
            ->with('sucesso', 'Super Administrador cadastrado com sucesso!');

    }

    public function postCriarArea(Request $request){

        $validator = Validator::make($request->all(), [
            'nome' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect('admin/locais')
                ->withErrors($validator)
                ->withInput()
                ->with('criar-area-fail',true);
        }else{
            Area::create($request->all());
            return redirect('admin/locais');
        }

    }

    public function postCriarLocal(Request $request){

        $validator = Validator::make($request->all(), [
            'nome' => 'required',
            'capacidade' => 'required|numeric',
            'descricao' => '',
            'area_id' => ''
        ]);

        if ($validator->fails()) {
            return redirect('admin/locais')
                ->withErrors($validator)
                ->withInput()
                ->with('criar-local-fail',true);
        }else{
            Sala::create($request->all());
            return redirect('admin/locais');
        }

    }


    public function getAlterarArea($id)
    {
        $area = Area::findOrFail($id);

        if(request()->ajax()){
            return response()->json([
                'success' => true,
                'formulario' => view('area.partials.alterar-form')->with('area', $area)->render()
            ]);
        }

        return view('area.alterar')->with(compact('area'));
    }

    public function postAlterarArea(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'nome' => 'required',
        ]);

        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }
        else{
            $area = Area::findOrFail($id);

            $area->nome = $request->nome;
            $area->save();

            if(request()->ajax()){
                return response()->json(['successo' => 'Área alterada com sucesso.']);
            }

            return back()->with('sucesso', 'Área alterada com sucesso.');
        }
    }

}
