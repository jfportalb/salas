<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserAlterarSenhaRequest;
use App\Http\Requests\UserRequest;
use App\Sala;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use Mail;
use Validator;

class UserController extends Controller
{
    /**
     * @return \Illuminate\View\View
     */
    public function getHome()
    {
        $data['salas']= Sala::all()->groupBy('area_id');
        return view("user/home")->with($data);
    }

    /**
     * @return \Illuminate\View\View
     */
    public function getListar(Request $request)
    {
        $usuarios = User::where('tipo_login', '=', 0)->where('status_login', '<>', -1)->paginate(10);
        $usuarios->setPath($request->url());

        return view('admin.listar', compact('usuarios'));
    }

    public function getUsuario($id)
    {
        $user = User::where('id', $id)->first();
        $user->setVisible(['nome', 'email', 'celular', 'ramal', 'sala']);

        return $user;
    }

    public function getListarAdmins()
    {
        if (Auth::user()->isSuperAdmin()){
            $usuarios = User::where('tipo_login', '<>', 0)->where('status_login', '<>', -1)->get();
            return view('admin.listar_admin', compact('usuarios'));
        } else
            abort(403);
    }

    /**
     * @param $id
     * @return \Illuminate\View\View
     */
    public function getAlterar($id)
    {
        $usuario = User::findOrFail($id);

        if(Auth::user()->tipo_login >= User::TYPE_ADMIN) {
            if(request()->ajax()){
                return response()->json([
                    'success' => true,
                    'formulario' => view('admin.partials.alterar-form')->with('usuario', $usuario)->render()
                ]);
            }

            return view('admin.alterar', compact('usuario'));
        }
        else {
            if(Auth::user()->id == $id){
                if(request()->ajax()){
                    return response()->json([
                        'success' => true,
                        'formulario' => view('admin.partials.alterar-form')->with('usuario', $usuario)->render()
                    ]);
                }

                return view('user.alterar', compact('usuario'));
            }
            else{
                abort(404, 'Unauthorized action.');
            }
        }
    }

    /**
     * @param UserRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postAlterar(Request $request, $id)
    {
        $this->validate($request, [
                'nome' => 'required|max:255',
                'email' => 'required|email|max:255',
                'celular' => 'required|min:8|max:15',
                'ramal' => 'required|numeric|min:0000|max:9999',
                'sala' => 'required|max:255'
        ]);

        DB::table('users')->where('id', $id)
            ->update($request->except(['_token', 'password_confirmation']));

        if(request()->ajax()){
            return response()->json(['sucesso' => 'Os dados foram alterados com sucesso!']);
        }

        return redirect()->back()->with("sucesso", 'Os dados foram alterados com sucesso!');
    }

    /**
     * Processa a alteração da senha de usuário
     *
     * @param UserAlterarSenhaRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postAlterarSenha(UserAlterarSenhaRequest $request)
    {
        $usuario = User::find($request->id);

        if (Hash::check($request->old_password, $usuario->password)) {
            DB::table('users')->where('id', $request->id)
                ->update($request->except(['_token', 'password_confirmation', 'old_password']));

            DB::table('users')->where('id', $request->id)
                ->update(['password' => bcrypt($request->password)]);

            return redirect()->back()->with("password_changed",true);
        }
        else {
            return redirect()->back()->withErrors("Senha incorreta")->with("password_not_changed",true);
        }
    }

    public function getDeletar($id)
    {
        $usuario = User::findOrFail($id);
        $loggedUser = Auth::user();

        if($loggedUser->isAdmin() && !$usuario->isAdmin()) {
            $usuario->delete();

            if(request()->ajax()){
                return response()->json(['success' => true]);
            }

            return back()->with('sucesso', 'Usuário deletado com sucesso!');
        }
        elseif ($loggedUser->isSuperAdmin() && $loggedUser->id != $usuario->id){
            if ($usuario->isSuperAdmin() && User::where('tipo_login', User::TYPE_SUPERADMIN)->count()<=User::MIN_SUPER_ADMINS){
                if(request()->ajax()){
                    return response()->json(['success' => false]);
                }

                return back()->with('error', 'Não se pode ter menos de '.User::MIN_SUPER_ADMINS.' super admins');
            }
            else {
                $usuario->delete();

                if(request()->ajax()){
                    return response()->json(['success' => true]);
                }

                return back()->with('sucesso', 'Usuário deletado com sucesso!');
            }
        }
        else {
            abort(404, 'Unauthorized action.');
        }
    }

    public function getCadastrar()
    {
        return view('admin.cadastrar-usuario');
    }

    public function postCadastrar(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email|max:255|unique:users',
            'nome' => 'required|max:255',
            'celular' => 'required|max:15',
            'ramal' => 'required|numeric',
            'sala' => 'required|max:255',
        ]);

        if ($validator->fails()){
            return back()->with('create_user_failed', true)->withErrors($validator)->withInput();
        }

        $user = User::create($request->all());
        $user->status_login = User::STATUS_ACTIVE;
        $user->save();

        $email = $user->email;
        Mail::send('emails.registered', [
                'nome'  => $user->nome,
                'email' => $user->email,
        ], function ($message) use ($email) {
            $message->to($email)
                    ->subject('Cadastro criado no Sistema');
        });

        return redirect(action('UserController@getListar'))
                ->with('sucesso', 'Usuário cadastrado com sucesso!');

    }

    public function getCriarSenha()
    {
        return view('user.criar-senha');
    }

    public function postCriarSenha(Request $request)
    {
        $this->validate($request, [
                'senha' => 'required | confirmed | min:6',
                'email' => 'required | email'
        ]);

        $usuario = User::where('email', $request->email)->first();

        if(!is_null($usuario)){
            if(!empty($usuario->password)){
                return back()->with('error', 'Uma senha já foi cadastrada para esse usuário.');
            }

            $usuario->password = bcrypt($request->senha);
            $usuario->save();

            return back()->with('sucesso', 'Senha cadastrada com sucesso! Faça login no sistema.');
        }
        else{
            return back()->with('error', 'Usuário não encontrado no sistema.');
        }
    }
}
