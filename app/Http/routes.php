<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

// Rotas para visitantes não logados
Route::group(['middleware' => 'guest'], function(){

    // Authentication routes...
    Route::get('/', 'Auth\AuthController@getLogin');
    Route::post('/login', 'Auth\AuthController@postLogin');

    // Registration routes...
//    Route::get('/cadastro', 'Auth\AuthController@getRegister');
//    Route::post('/cadastro', 'Auth\AuthController@postRegister');

    // Rotas de recuperação de senha
    Route::get('/recuperar-senha', 'Auth\PasswordController@getEmail');
    Route::post('/recuperar-senha', 'Auth\PasswordController@postEmail');

    Route::get('/recriar-senha/{token}', 'Auth\PasswordController@getReset');
    Route::post('/recriar-senha', 'Auth\PasswordController@postReset');

    // Rotas para criar senha para usuário cadastrado por admin
    Route::get('/criar-senha', 'UserController@getCriarSenha');
    Route::post('/criar-senha', 'UserController@postCriarSenha');
});

// Rotas para usuários logados no sistema
Route::group(['middleware' => 'auth'], function(){

    // Rota de Logout do usuario
    Route::get('/logout', 'Auth\AuthController@getLogout');

    // Pagina de alteracao de dados dos usuarios
    Route::get('/alterar-dados/{id}', 'UserController@getAlterar');
    Route::post('/alterar-dados/{id}', 'UserController@postAlterar');

    // Pagina de alteracao de senha dos usuarios
    Route::get('/alterar-senha/{id}', 'UserController@getAlterarSenha');
    Route::post('/alterar-senha/{id}', 'UserController@postAlterarSenha');

    //Rota para ver as reservas de uma sala:
    Route::get('reservas/{sala}', ['as' => 'verReservasSala', 'uses' => 'ReservasController@getReservasFromSala']);
    Route::get('pedidos/{sala}', ['as' => 'verPedidosSala', 'uses' => 'ReservasController@getPedidosFromSala'])->where('sala', '[0-9]+');

    // Rotas para usuários COM permisão administrativa
    Route::group(['middleware' => 'admin', 'prefix' => 'admin'], function() {

        // Admin home page
        Route::get('/home', ['as' => 'adminHome', 'uses' => 'AdminController@getHome']);
        Route::post('/home', 'AdminController@postAlterarPendenciaUsuario');

        // Pagina de listagem dos usuarios
        Route::get('/usuarios', 'UserController@getListar');
        Route::post('/get-usuario/{id}', 'UserController@getUsuario');
        Route::get('/admins', 'UserController@getListarAdmins');
        Route::get('/usuarios/cadastrar', 'UserController@getCadastrar');
        Route::post('/usuarios', 'UserController@postCadastrar');
        Route::get('/usuarios/cadastrar-admin', 'AdminController@getCadastrar');
        Route::post('/admins', 'AdminController@postCadastrar');
        Route::post('/adminsSuper', 'AdminController@postCadastrarSuper');
        Route::get('/usuarios/{id}', 'UserController@getDeletar');

        //Pagina de recursos
        Route::get('/recursos', 'RecursosController@getTipoRecursos');
        Route::get('/getRecursos/{id}', [ 'as' => 'getRecursos', 'uses' => 'RecursosController@getRecursos']);
        Route::put('/createRecurso', [ 'as' => 'criarRecurso', 'uses' => 'RecursosController@postCriarRecurso']);
        Route::post('/tipo_recursos', [ 'as' => 'criarTipoRecurso', 'uses' => 'RecursosController@postCriarTipoRecurso']);
        Route::get('/recursos/adicionar', [ 'as' => 'adicionarRecurso', 'uses' => 'RecursosController@postCriarRecurso']);
        Route::get('/recursos/alterar/{id}', 'RecursosController@getAlterar');
        Route::post('/recursos/alterar/{id}', 'RecursosController@postAlterar');
        Route::get('/recursos/deletar/{id}', [ 'as' => 'removerTipoRecurso', 'uses' => 'RecursosController@getDeletarTipo']);

        // Pagina de listagem dos locais
        Route::get('/locais', 'AdminController@getLocais');
        Route::post('/locais', 'AdminController@getLocal');
        Route::post('/criar_local', 'AdminController@postCriarLocal');
        Route::get('/locais/alterar/{id}', 'AdminController@getAlterarLocal');
        Route::post('/locais/alterar/{id}', 'AdminController@postAlterarLocal');
        Route::get('/locais/deletar/{id}', 'AdminController@getDeletarLocal');
        Route::get('/locais/area/deletar/{id}', 'AdminController@getDeletarArea');
        Route::get('/locais/area/alterar/{id}', 'AdminController@getAlterarArea');
        Route::post('/locais/area/alterar/{id}', 'AdminController@postAlterarArea');

        Route::post('/criar_local', 'AdminController@postCriarLocal');

        Route::post('/criar_area', 'AdminController@postCriarArea');

        //Página de listagem de pedidos/reservas
        Route::get('/pendencias', 'ReservasController@getPendenciasReserva');
        Route::post('/pendencias', ['as' => 'avaliarReserva', 'uses' => 'ReservasController@postAvaliarReserva']);

        Route::get('/pendencias/pedido/{id}', 'ReservasController@getVisualizarPedido');
    });

    // Rotas para usuários SEM permisão administrativa
    Route::group(['middleware' => 'user', 'prefix' => 'user'], function() {

        Route::get('/home', ['as' => 'userHome', 'uses' => 'UserController@getHome']);
        Route::post('/home/criar-reserva', ['as' => 'criar-reserva', 'uses' => 'ReservasController@putPedido']);

        //Rotas de pedidos/reservas
        Route::put('/pedidos/create', ['as' => 'putPedido', 'uses' => 'ReservasController@putPedido']);
        Route::get('/pedidos', ['as' => 'userPedidos', 'uses' => 'ReservasController@getUserPedidos']);
        Route::get('/pedidos/{id}', 'ReservasController@getVisualizarPedido');
        Route::get('/pedidos/cancelar/{id}', ['as' => 'cancelarPedido', 'uses' => 'ReservasController@getCancelarPedido']);
    });

});