<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pedido extends Model
{

    //Valores do status de aprovação:
    const STATUS_PENDENTE = 0;
    const STATUS_ACEITO = 1;
    const STATUS_RECUSADO = 2;

    const COR_PEDIDO_ACEITO = '#21a4da';
    const COR_PEDIDO_PENDENTE = '#5E0000';
    const COR_PEDIDO_SENDO_FEITO = "#AC1008";

    protected $table = 'pedidos';

    protected $fillable = ['title', 'descricao', 'user_id', 'sala_id', 'status_aprovacao'];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function sala()
    {
        return $this->belongsTo('App\Sala');
    }

    public function reservas()
    {
        return $this->hasMany('App\Reserva');
    }

    public function accept(){
        $this->update([
            'status_aprovacao' => Pedido::STATUS_ACEITO
        ]);
    }

    public function reject(){
        $this->update([
            'status_aprovacao' => Pedido::STATUS_RECUSADO
        ]);
        Reserva::where('pedido_id', $this->id)->delete();
    }

    public function isActive(){
        return $this->status_aprovacao === Pedido::STATUS_ACEITO;
    }
}
