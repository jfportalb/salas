<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\Access\Authorizable;

class User extends Model implements AuthenticatableContract, AuthorizableContract, CanResetPasswordContract
{
    use Authenticatable, Authorizable, CanResetPassword, SoftDeletes;

    const MIN_SUPER_ADMINS = 2;

    //Type Values
    const TYPE_SUPERADMIN = 2;
    const TYPE_ADMIN = 1;
    const TYPE_USUARIO = 0;

    //Status Values
    const STATUS_UNACTIVE = 0;
    const STATUS_ACTIVE = 1;
    const STATUS_REPROVED = -1;

    protected $table = 'users';

    protected $fillable = ['email', 'nome', 'celular', 'ramal', 'sala'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];

    public function reservas()
    {
        return $this->hasMany('App\Reserva');
    }

    public function pedidos()
    {
        return $this->hasMany('App\Pedido');
    }

    public function areas()
    {
        return $this->belongsToMany('App\Area', 'areas_users');
    }

    public function isAdmin()
    {
        return $this->tipo_login != User::TYPE_USUARIO;
    }

    public function isNormalAdmin()
    {
        return $this->tipo_login == User::TYPE_ADMIN;
    }

    public function isSuperAdmin()
    {
        return $this->tipo_login == User::TYPE_SUPERADMIN;
    }
}
