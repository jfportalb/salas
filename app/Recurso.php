<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Recurso extends Model
{
    protected $table = 'recursos';

    protected $fillable = ['codigo', 'sala'];
	protected $visible = ['codigo', 'sala'];

    public function sala()
    {
        return $this->belongsTo('App\Sala');
    }

    public function tipoRecurso()
    {
        return $this->belongsTo('App\TipoRecurso');
    }

}
