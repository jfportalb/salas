<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reserva extends Model
{

    protected $table = 'reservas';

    protected $fillable = ['start', 'end', 'pedido_id'];

    protected $visible = ['title', 'start', 'end', 'color', 'overlap'];
    protected $appends = ['color', 'overlap', 'title'];

    public function user()
    {
        return $this->pedido->user();
    }

    public function pedido()
    {
        return $this->belongsTo('App\Pedido');
    }

    public function allActive()
    {
        return $this->filter(function($reserva){
            $reserva->isActive();
        });
    }

    public function isActive(){
        return $this->pedido->isActive();
    }

    public function getColorAttribute()
    {
        return $this->pedido->attributes['status_aprovacao'] == Pedido::STATUS_PENDENTE ? Pedido::COR_PEDIDO_PENDENTE : Pedido::COR_PEDIDO_ACEITO;
    }

    public function getOverlapAttribute()
    {
        return $this->pedido->attributes['status_aprovacao'] == Pedido::STATUS_PENDENTE ? true : false;
    }

    public function getTitleAttribute()
    {
        return $this->pedido->title;
    }
}
