<?php

use App\User;
use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{

    public function run()
    {
		DB::table('users')->delete();

        for ($i = 0; $i<User::MIN_SUPER_ADMINS; $i++)
            factory(User::class)->create([
                'email' => 'super'.$i.'@email.com',
                'password' => bcrypt('123456'),
                'tipo_login' => User::TYPE_SUPERADMIN,
                'status_login' => User::STATUS_ACTIVE,
            ]);

        factory(User::class)->create([
			'email' => 'admin@email.com',
			'password' => bcrypt('123456'),
			'tipo_login' => User::TYPE_ADMIN,
			'status_login' => User::STATUS_ACTIVE,
        ]);

        factory(User::class)->create([
            'email' => 'user@email.com',
            'password' => bcrypt('123456'),
            'tipo_login' => User::TYPE_USUARIO,
            'status_login' => User::STATUS_ACTIVE,
        ]);

        factory(User::class, 10)->create();
    }
}
