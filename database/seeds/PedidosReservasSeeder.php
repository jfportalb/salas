<?php

use App\Pedido;
use App\Reserva;
use Illuminate\Database\Seeder;

class PedidosReservasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('pedidos')->delete();
        DB::table('reservas')->delete();

        factory(Pedido::class, 10)->create([
            'status_aprovacao' => Pedido::STATUS_PENDENTE,
        ]);

        factory(Pedido::class, 10)->create([
            'status_aprovacao' => Pedido::STATUS_ACEITO,
        ]);

        factory(Reserva::class, 100)->create();
    }
}
