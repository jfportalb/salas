<?php

use App\Area;
use App\Sala;

use Illuminate\Database\Seeder;

class AreasSalasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('salas')->delete();
        DB::table('areas')->delete();

        factory(Area::class)->create([
            'nome' => 'Anfiteatros',
        ]);
        factory(Area::class)->create([
            'nome' => 'Laboratórios',
        ]);
        factory(Area::class)->create([
            'nome' => 'Copas',
        ]);

        factory(Sala::class)->create([
            'area_id' => 1,
            'nome' => 'Espaço Flex',
            'capacidade' => 50,
            'descricao' => 'Espaço flex de baixo da escada do MOT.',
        ]);

        factory(Sala::class)->create([
            'area_id' => 1,
            'nome' => 'SALA DE REUNIÃO ASI',
            'capacidade' => 10,
        ]);

        factory(Sala::class)->create([
            'area_id' => 2,
            'nome' => 'LABORATORIO 8',
            'capacidade' => 19,
        ]);

        factory(Sala::class)->create([
            'area_id' => 1,
            'nome' => 'ANFITEATRO MBI ( JAIME SZWARCFITER )',
            'capacidade' => 30,
        ]);

        factory(Sala::class)->create([
            'area_id' => 2,
            'nome' => 'LABORATÓRIO 4',
            'capacidade' => 18,
            'descricao' => 'LABORATÓRIO COM RACK DE ROTEADORES E SWITCHES PARA AULAS DE REDES (USUALMENTE CISCO/MOT/MSI) OBS.NÃO TEMOS AR NO MOMENTO ESTAMOS USANDO VENTILADORES,PROJETOR QUEIMADO!',
        ]);

        factory(Sala::class)->create([
            'area_id' => 3,
            'nome' => 'COPA DA BAIANA (REFEITÓRIO)',
            'capacidade' => 70,
            'descricao' => '35 MESAS E 70 CADEIRAS 01 TELEVISOR 29 POLEGADAS',
        ]);

        factory(Sala::class)->create([
            'area_id' => 3,
            'nome' => 'COPA PARA USO EXTERNO',
            'capacidade' => 0,
            'descricao' => '01 GELADEIRA 02 MICRO ONDAS 01 FOGÃO INDUSTRIAL',
        ]);
    }
}
