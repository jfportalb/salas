<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		Model::unguard();

		$this->call('UserTableSeeder');

		$this->command->info('User table seeded!');

        $this->call('AreasSalasTableSeeder');

        $this->command->info('Areas / Salas table seeded!');

//        $this->call('PedidosReservasSeeder');
//
//        $this->command->info('Pedidos / Reservas table seeded!');

		Model::reguard();
    }
}
