<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePedidosTable extends Migration
{
    public function up()
    {
        Schema::create('pedidos', function (Blueprint $table) {

            $table->increments('id');

            $table->tinyInteger("status_aprovacao")->unsigned();

            $table->integer('user_id')->unsigned();
            $table->integer('sala_id')->unsigned();

            $table->string('title');
            $table->text('descricao');

            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('sala_id')->references('id')->on('salas');

            $table->timestamps();
            $table->softDeletes();
        });
    }

    public function down()
    {
        Schema::drop('pedidos');
    }
}
