<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSalasTable extends Migration
{

    public function up()
    {
        Schema::create('salas', function (Blueprint $table) {

            $table->increments('id');

            $table->integer('area_id')->unsigned();

            $table->string('nome');
            $table->tinyInteger("capacidade");
            $table->string('descricao');

            $table->foreign('area_id')->references('id')->on('areas');

            $table->timestamps();
            $table->softDeletes();
        });
    }

    public function down()
    {
        Schema::drop('salas');
    }
}
