<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {

			$table->increments('id');

			$table->string("email")->unique();
			$table->string("password");
			$table->tinyInteger("tipo_login")->unsigned();
			$table->tinyInteger("status_login")->unsigned();

			$table->string('nome');
			$table->string("celular");
			$table->integer("ramal");
			$table->string('sala');

			$table->timestamps();
			$table->softDeletes();
			$table->rememberToken();
        });
    }
    public function down()
    {
        Schema::drop('users');
    }
}
