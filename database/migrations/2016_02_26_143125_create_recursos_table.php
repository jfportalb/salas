<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRecursosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('recursos', function (Blueprint $table) {

            $table->increments('id');

            $table->string('codigo');
            $table->integer('tipo_recurso_id')->unsigned();
            $table->integer('sala_id')->unsigned()->nullable();

            $table->foreign('tipo_recurso_id')->references('id')->on('tipo_recursos');
            $table->foreign('sala_id')->references('id')->on('salas');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('recursos');
    }
}
