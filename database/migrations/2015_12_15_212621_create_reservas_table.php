<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReservasTable extends Migration
{

    public function up()
    {
        Schema::create('reservas', function (Blueprint $table) {

            $table->increments('id');

            $table->integer('pedido_id')->unsigned();

            $table->timestamp('start');
            $table->timestamp('end');

            $table->foreign('pedido_id')->references('id')->on('pedidos')->onDelete('cascade');

            $table->timestamps();
            $table->softDeletes();
        });
    }

    public function down()
    {
        Schema::drop('reservas');
    }
}
