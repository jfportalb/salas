<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

use App\User;
use App\Area;

$factory->define(App\User::class, function (Faker\Generator $faker) {
    return [
		'email' => $faker->email,
		'password' => bcrypt('123456'),
		'tipo_login' => User::TYPE_USUARIO,
		'status_login' => rand(User::STATUS_UNACTIVE,User::STATUS_ACTIVE),
        'nome' => $faker->name,
		'celular' => $faker->phoneNumber,
		'ramal' => rand(1000,9999),
		'sala' => rand(0,100),
    ];
});

$factory->define(App\Area::class, function (Faker\Generator $faker) {
	return [
		'nome' => $faker->name,
	];
});

$factory->define(App\Sala::class, function (Faker\Generator $faker) {
	return [
		'nome' => $faker->name,
		'capacidade' => $faker->numberBetween(0,50),
		'descricao' => $faker->realText(),
		'area_id' => \App\Area::orderByRaw("RAND()")->first()->id,
	];
});

$factory->define(App\Pedido::class, function (Faker\Generator $faker) {
	return [
		'title' => $faker->name,
		'descricao' => $faker->realText(),
		'user_id' => User::orderByRaw("RAND()")->first()->id,
		'status_aprovacao' => $faker->numberBetween(0, 2)
	];
});

$factory->define(App\Reserva::class, function (Faker\Generator $faker) {
	$start =  $faker->dateTimeBetween($startDate = 'now', $endDate = '+1 month')->getTimeStamp();
	$end = $faker->dateTimeBetween($startDate = $start, $endDate = $start.' +1 day')->getTimeStamp();
	return [
		'title' => $faker->name,
		'start' => $start,
		'end' => $end,
		'sala_id' => \App\Sala::orderByRaw("RAND()")->first()->id,
		'pedido_id' => \App\Pedido::orderByRaw("RAND()")->first()->id,
	];
});
